#!/bin/bash

# Define colors
GREEN='\033[0;32m'
YELLOW='\033[1;33m'
BLUE='\033[0;34m'
RED='\033[0;31m'
CYAN='\033[1;36m'
RESET='\e[0m'
BOLD_WHITE='\e[1;37m'
BOLD_LIGHT_BLUE='\e[1;94m'
BOLD_PURPLE='\e[1;35m'
BOLD_GREEN='\e[1;32m'
BOLD_BROWN='\e[0;33m'
BOLD_RED='\e[1;31m'
BOLD_YELLOW='\e[1;33m'

# Check and install required packages
REQUIRED_PKGS=("btrfs-progs" "inotify-tools" "grub-btrfs" "timeshift")
MISSING_PKGS=()
for pkg in "${REQUIRED_PKGS[@]}"; do
    if ! pacman -Q "$pkg" &>/dev/null; then
        MISSING_PKGS+=("$pkg")
    fi
done
if [[ ${#MISSING_PKGS[@]} -ne 0 ]]; then
    printf "%b" "${BOLD_RED}❌ Missing packages detected. Installing: ${MISSING_PKGS[*]}${RESET}\n"
    sudo pacman -S --noconfirm "${MISSING_PKGS[@]}"
else
    printf "%b" "${BOLD_GREEN}✅ All required packages are already installed.${RESET}\n"
fi

# Check and configure grub-btrfsd service
if ! systemctl is-active --quiet grub-btrfsd.service; then
    printf "%b" "${BOLD_BLUE}ℹ️ grub-btrfsd service is not active, configuring...${RESET}\n"
    sudo tee /etc/systemd/system/grub-btrfsd.service > /dev/null <<EOF
[Unit]
Description=Regenerate grub-btrfs.cfg

[Service]
Type=simple
LogLevelMax=notice
Environment="PATH=/sbin:/bin:/usr/sbin:/usr/bin"
ExecStart=/usr/bin/grub-btrfsd --syslog --timeshift-auto

[Install]
WantedBy=multi-user.target
EOF
    sudo systemctl daemon-reload
    sudo systemctl enable --now grub-btrfsd
    printf "%b" "${BOLD_GREEN}✅ grub-btrfsd service has been enabled.${RESET}\n"
else
    printf "%b" "${BOLD_GREEN}✅ grub-btrfsd service is already active.${RESET}\n"
fi

# Function to display the main menu
main_menu() {
    while true; do
        printf "%b" "${BOLD_PURPLE}╭── ⋅ ── ✩ ── ⋅ ──╮${RESET}\n"
        printf "%b" "${BOLD_LIGHT_BLUE}✨ Welcome to the Btrfs Snapshot Management ✨${RESET}\n"
        printf "%b" "${BOLD_PURPLE}╰── ⋅ ── ✩ ── ⋅ ──╯${RESET}\n"

        # Display the menu options
        printf "%b" "${BOLD_WHITE}💡 Select an option from the menu:${RESET}\n"
        printf "%b" "${BOLD_GREEN}1.${BOLD_LIGHT_BLUE} Create a new Btrfs snapshot with Timeshift${RESET}\n"
        printf "%b" "${BOLD_YELLOW}2.${BOLD_GREEN} List existing Btrfs snapshots and restore${RESET}\n"
        printf "%b" "${BOLD_RED}3.${BOLD_BROWN} Delete a Btrfs snapshot${RESET}\n"
        printf "%b" "${BOLD_PURPLE}4.${BOLD_CYAN} Exit${RESET}\n"
        printf "%b" "${BOLD_PURPLE}☆.。.:*・°☆.。.:*・°☆.。.:*・°☆.。.:*・°☆${RESET}\n"

        # Read user choice with color reset before the prompt
        printf "%b" "${BOLD_YELLOW}❓ Select an option (1/2/3/4): ${RESET}"
        read -r choice
        echo -e "${RESET}"

        case "$choice" in
            1)
                create_snapshot
                break
                ;;
            2)
                list_restore_snapshot
                break
                ;;
            3)
                delete_snapshot
                break
                ;;
            4)
                printf "%b" "${BOLD_GREEN}ℹ️ Exiting the program.${RESET}\n"
                exit 0
                ;;
            *)
                printf "%b" "${BOLD_RED}❌ Invalid selection. Please choose a valid option (1/2/3/4).${RESET}\n"
                ;;
        esac
    done
}

create_snapshot() {
    clear
    printf "%b" "${BOLD_CYAN}🖥️ ${BOLD_LIGHT_BLUE}Enter a comment for the snapshot: ${RESET}"
    read -r comment
    printf "%b\n" "${BOLD_GREEN}💬 Snapshot comment: ${BOLD_YELLOW}$comment${RESET}"

    # Create the snapshot
    printf "%b\n" "${BOLD_CYAN}📸 Creating snapshot with comment: ${BOLD_LIGHT_BLUE}$comment${RESET}..."
    sudo timeshift --create --btrfs --comments "$comment" --tags D
    printf "%b\n" "${BOLD_GREEN}✅ ${BOLD_WHITE}Snapshot successfully created!${RESET}"

    # Check if GRUB service is running
    if ! systemctl is-active --quiet grub-btrfsd.service; then
        printf "%b\n" "${BOLD_YELLOW}⚙️ GRUB service is not active, updating GRUB configuration...${RESET}"
        sudo grub-mkconfig -o /boot/grub/grub.cfg
        printf "%b\n" "${BOLD_BROWN}🔄 GRUB configuration updated successfully!${RESET}"
    else
        printf "%b\n" "${BOLD_CYAN}✅ ${BOLD_WHITE}GRUB service is already active. No need for an update.${RESET}"
    fi

    # Confirmation and next steps
    printf "%b\n" "${BOLD_GREEN}🎉 Snapshot creation complete! Now you can continue your work! 🚀${RESET}"
    
    # Proceed to the main menu after a brief pause
    printf "\n🎯 ${BOLD_GREEN}Press Enter to return to the main menu... ${RESET}"
    read -r
    main_menu
}

list_restore_snapshot() {
    clear
    printf "%b\n" "${BOLD_CYAN}📈 ${BOLD_LIGHT_BLUE}Listing existing snapshots...${RESET}"

    # Timeshift çıktısını alıyoruz
    ts_out=$(sudo timeshift --list)

    ## 1. Satırlardaki Gereksiz Verileri Temizleyelim
    snapshots=$(echo "$ts_out" | awk 'NR>1 {if ($1 ~ /^[0-9]+$/) {print $0}}')

    if [ -z "$snapshots" ]; then
        printf "\n${BOLD_RED}❌ No snapshots found!${RESET}\n"
        back_to_menu
        return
    fi

    ## 2. Satırları Diziye Toplayalım (Her satır: "Num|Snapshot Name|Tags|Description")
    rows=()
    while IFS= read -r line; do
        [ -z "$line" ] && continue
        read -r num arrow snap tag desc <<< "$line"
        rows+=("$num|$snap|$tag|$desc")
    done <<< "$snapshots"

    ## 3. Dinamik Sütun Genişliklerini Hesaplayalım
    header1="Num"
    header2="Snapshot Name"
    header3="Tags"
    header4="Description"

    width1=${#header1}
    width2=${#header2}
    width3=${#header3}
    width4=${#header4}

    for row in "${rows[@]}"; do
        IFS='|' read -r col1 col2 col3 col4 <<< "$row"
        [ ${#col1} -gt $width1 ] && width1=${#col1}
        [ ${#col2} -gt $width2 ] && width2=${#col2}
        [ ${#col3} -gt $width3 ] && width3=${#col3}
        [ ${#col4} -gt $width4 ] && width4=${#col4}
    done

    # Her sütun için 2 boşluk (sol ve sağ)
    width1=$((width1 + 2))
    width2=$((width2 + 2))
    width3=$((width3 + 2))
    width4=$((width4 + 2))

    ## 4. Tablo Çerçeveleri (Alternatif ASCII karakterleri kullanılarak)
    top_border="+$(repeat "-" $width1)+$(repeat "-" $width2)+$(repeat "-" $width3)+$(repeat "-" $width4)+" 
    mid_border="+$(repeat "-" $width1)+$(repeat "-" $width2)+$(repeat "-" $width3)+$(repeat "-" $width4)+" 
    bottom_border="+$(repeat "-" $width1)+$(repeat "-" $width2)+$(repeat "-" $width3)+$(repeat "-" $width4)+" 

    ## 5. Tabloyu Yazdıralım
    echo "$top_border"
    printf "| %-*s| %-*s| %-*s| %-*s|\n" "$width1" "$header1" "$width2" "$header2" "$width3" "$header3" "$width4" "$header4"
    echo "$mid_border"

    for row in "${rows[@]}"; do
        IFS='|' read -r col1 col2 col3 col4 <<< "$row"
        printf "| %-*s| %-*s| %-*s| %-*s|\n" "$width1" "$col1" "$width2" "$col2" "$width3" "$col3" "$width4" "$col4"
    done
    echo "$bottom_border"

    ## 6. Kullanıcı Seçimi
    printf "\n${BOLD_YELLOW}❓ ${BOLD_LIGHT_BLUE}Enter the snapshot number to restore (or type 'back' to return to the menu): ${RESET}"
    read -r snapshot_num_input
    if [ "$snapshot_num_input" = "back" ]; then
        main_menu
        return
    fi

    selected=""
    for row in "${rows[@]}"; do
        IFS='|' read -r col1 col2 col3 col4 <<< "$row"
        if [ "$col1" = "$snapshot_num_input" ]; then
            selected="$row"
            break
        fi
    done

    if [ -z "$selected" ]; then
        printf "\n${BOLD_RED}❌ ${BOLD_WHITE}Invalid snapshot number. Please try again!${RESET}\n"
        back_to_menu
        return
    fi

    IFS='|' read -r sel_num sel_snap sel_tag sel_desc <<< "$selected"
    actual_snap_name=$(echo "$sel_snap" | sed 's/^ *//; s/ *$//')

    printf "\n${BOLD_GREEN}ℹ️ ${BOLD_CYAN}You are about to restore snapshot: ${BOLD_YELLOW}%s${RESET}\n" "$actual_snap_name"
    printf "${BOLD_YELLOW}❓ ${BOLD_WHITE}Are you sure you want to restore snapshot '%s'? (y/n): ${RESET}" "$actual_snap_name"
    read -r confirm_restore
    if [[ "$confirm_restore" =~ ^[yY]$ ]]; then
        sudo timeshift --restore --snapshot "$actual_snap_name" --yes
        printf "\n${BOLD_GREEN}✅ ${BOLD_CYAN}Snapshot '%s' restored successfully!${RESET}\n" "$actual_snap_name"
        printf "\n${BOLD_CYAN}ℹ️ ${BOLD_YELLOW}Reboot and select the snapshot from the GRUB menu to complete the restore process.${RESET}\n"
    else
        printf "\n${BOLD_RED}❌ ${BOLD_WHITE}Restore cancelled!${RESET}\n"
    fi

    printf "\n🎯 ${BOLD_GREEN}Press Enter to return to the main menu... ${RESET}"
    read -r
    main_menu
}

# Yardımcı fonksiyon: Belirtilen karakteri N kere tekrarlar.
repeat() {
    local char=$1
    local count=$2
    printf "%*s" "$count" "" | tr ' ' "$char"
}

delete_snapshot() {
    clear
    printf "%b\n" "${BOLD_CYAN}📸 ${BOLD_LIGHT_BLUE}Listing existing Btrfs snapshots...${RESET}"

    # Timeshift çıktısını alıyoruz
    ts_out=$(sudo timeshift --list)

    ## 1. Özet Satırı
    summary=$(echo "$ts_out" | grep -m1 "snapshots,")
    if [ -n "$summary" ]; then
        summary_name=$(echo "$summary" | awk '{print $2}' | sed 's/,$//')
    fi

    ## 2. Detay Satırları
    details=$(echo "$ts_out" | awk '/^[-]+/ {p=1; next} p && /^[0-9]+/ {print}')

    if [ -z "$summary" ] && [ -z "$details" ]; then
        printf "\n${BOLD_RED}❌ No snapshots found!${RESET}\n"
        back_to_menu
        return
    fi

    ## 3. Satırları Diziye Toplayalım (Her satır: "Num|Snapshot Name|Tags|Description")
    rows=()
    while IFS= read -r line; do
        [ -z "$line" ] && continue
        read -r num arrow snap tag desc <<< "$line"
        rows+=("$num|$snap|$tag|$desc")
    done <<< "$details"

    ## 4. Dinamik Sütun Genişliklerini Hesaplayalım
    header1="Num"
    header2="Snapshot Name"
    header3="Tags"
    header4="Description"

    width1=${#header1}
    width2=${#header2}
    width3=${#header3}
    width4=${#header4}

    for row in "${rows[@]}"; do
        IFS='|' read -r col1 col2 col3 col4 <<< "$row"
        [ ${#col1} -gt $width1 ] && width1=${#col1}
        [ ${#col2} -gt $width2 ] && width2=${#col2}
        [ ${#col3} -gt $width3 ] && width3=${#col3}
        [ ${#col4} -gt $width4 ] && width4=${#col4}
    done

    # Her sütun için 2 boşluk (sol ve sağ)
    width1=$((width1 + 2))
    width2=$((width2 + 2))
    width3=$((width3 + 2))
    width4=$((width4 + 2))

    ## 5. Tablo Çerçeveleri (Alternatif ASCII karakterleri kullanılarak)
    top_border="+$(repeat "-" $width1)+$(repeat "-" $width2)+$(repeat "-" $width3)+$(repeat "-" $width4)+" 
    mid_border="+$(repeat "-" $width1)+$(repeat "-" $width2)+$(repeat "-" $width3)+$(repeat "-" $width4)+" 
    bottom_border="+$(repeat "-" $width1)+$(repeat "-" $width2)+$(repeat "-" $width3)+$(repeat "-" $width4)+" 

    ## 6. Tabloyu Yazdıralım
    echo "$top_border"
    printf "| %-*s| %-*s| %-*s| %-*s|\n" "$width1" "$header1" "$width2" "$header2" "$width3" "$header3" "$width4" "$header4"
    echo "$mid_border"

    for row in "${rows[@]}"; do
        IFS='|' read -r col1 col2 col3 col4 <<< "$row"
        printf "| %-*s| %-*s| %-*s| %-*s|\n" "$width1" "$col1" "$width2" "$col2" "$width3" "$col3" "$width4" "$col4"
    done
    echo "$bottom_border"

    ## 7. Kullanıcı Seçimi
    printf "\n${BOLD_YELLOW}❓ ${BOLD_LIGHT_BLUE}Enter the snapshot number to delete (or type 'back' to return to the menu): ${RESET}"
    read -r snapshot_num_input
    if [ "$snapshot_num_input" = "back" ]; then
        main_menu
        return
    fi

    selected=""
    for row in "${rows[@]}"; do
        IFS='|' read -r col1 col2 col3 col4 <<< "$row"
        if [ "$col1" = "$snapshot_num_input" ]; then
            selected="$row"
            break
        fi
    done

    if [ -z "$selected" ]; then
        printf "\n${BOLD_RED}❌ ${BOLD_WHITE}Invalid snapshot number. Please try again!${RESET}\n"
        back_to_menu
        return
    fi

    IFS='|' read -r sel_num sel_snap sel_tag sel_desc <<< "$selected"
    actual_snap_name=$(echo "$sel_snap" | sed 's/^ *//; s/ *$//')

    printf "\n${BOLD_GREEN}ℹ️ ${BOLD_CYAN}You are about to delete snapshot: ${BOLD_YELLOW}%s${RESET}\n" "$actual_snap_name"
    printf "${BOLD_YELLOW}❓ ${BOLD_WHITE}Are you sure you want to delete snapshot '%s'? (y/n): ${RESET}" "$actual_snap_name"
    read -r confirm_delete
    if [[ "$confirm_delete" =~ ^[yY]$ ]]; then
        sudo timeshift --delete --snapshot "$actual_snap_name" --scripted >/dev/null 2>&1
        printf "\n${BOLD_GREEN}✅ ${BOLD_CYAN}Snapshot '%s' deleted successfully!${RESET}\n" "$actual_snap_name"
    else
        printf "\n${BOLD_RED}❌ ${BOLD_WHITE}Deletion cancelled!${RESET}\n"
    fi

    printf "\n🎯 ${BOLD_GREEN}Press Enter to return to the main menu... ${RESET}"
    read -r
    main_menu
}

# Function to go back to the main menu
back_to_menu() {
    printf "%b" "${BOLD_YELLOW}🎯 Press Enter to return to the main menu...${RESET}"
    read -r
    main_menu
}

# Start the main menu
main_menu

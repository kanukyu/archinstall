#!/bin/bash

# Color codes (Normal and Bold)
BLACK='\033[0;30m'        # Black
DARK_GRAY='\033[1;30m'    # Dark Gray
RED='\033[0;31m'          # Red
LIGHT_RED='\033[1;31m'    # Light Red
GREEN='\033[0;32m'        # Green
LIGHT_GREEN='\033[1;32m'  # Light Green
BROWN_ORANGE='\033[0;33m' # Brown/Orange
YELLOW='\033[1;33m'       # Yellow
BLUE='\033[0;34m'         # Blue
LIGHT_BLUE='\033[1;34m'   # Light Blue
PURPLE='\033[0;35m'       # Purple
LIGHT_PURPLE='\033[1;35m' # Light Purple
CYAN='\033[0;36m'         # Cyan
LIGHT_CYAN='\033[1;36m'   # Light Cyan
LIGHT_GRAY='\033[0;37m'   # Light Gray
WHITE='\033[1;37m'        # White
RESET='\033[0m'

# Emojis
SUCCESS_EMOJI="✅"        # Success emoji
ERROR_EMOJI="❌"          # Error emoji
INFO_EMOJI="ℹ️"          # Info emoji
WARNING_EMOJI="⚠️"       # Warning emoji
QUESTION_EMOJI="❓"       # Question emoji
DONE_EMOJI="✔️"          # Done emoji
EXCLAMATION_EMOJI="❗"    # Exclamation emoji
CHECKBOX_EMOJI="☑️"      # Checkbox emoji
CROSS_MARK_EMOJI="❎"     # Cross mark emoji
FINGER_POINT_EMOJI="👉"  # Finger point emoji

# Text Symbols
ARROW_RIGHT="→"       # Right arrow
ARROW_LEFT="←"        # Left arrow
ARROW_UP="↑"          # Up arrow
ARROW_DOWN="↓"        # Down arrow
DOUBLE_ARROW_RIGHT="⟶" # Double right arrow
CHECK_MARK="✔"        # Check mark
CROSS_MARK="✘"        # Cross mark
PLUS_SIGN="+"         # Plus sign
DIVISION_SIGN="÷"     # Division sign
PIPE_SYMBOL="|"       # Pipe symbol

KANUKYU=" 
    __                         __                
   / /__ ____ _ ____   __  __ / /__ __  __ __  __
  / //_// __ \`// __ \ / / / // //_// / / // / / /
 / ,<  / /_/ // / / // /_/ // ,<  / /_/ // /_/ / 
/_/|_| \__,_//_/ /_/ \__,_//_/|_| \__, / \__,_/  
                                 /____/          
"

# Menu display with emoji, color, and symbols
show_menu() {
    echo -e "${BOLD_CYAN}$KANUKYU$RESET"
    echo -e "${YELLOW}${ARROW_UP} ${BOLD_BLUE}Welcome to VMware Tools Setup ${ARROW_DOWN}${RESET}"
    echo -e "${INFO_EMOJI}${BOLD_CYAN} Choose an option from the menu below: ${INFO_EMOJI}"
    echo -e "1) ${LIGHT_GREEN}${ARROW_RIGHT} Configure Open VM Tools and Shared Folder Host to Guest ${INFO_EMOJI}"
    echo -e "2) ${LIGHT_RED}${ARROW_RIGHT} Remove Open VM Tools and Revert All Changes From The First Selection ${CROSS_MARK_EMOJI}"
    echo -e "3) ${BOLD_WHITE}${ARROW_RIGHT} TikTok/Kanukyu ${STAR_EMOJI}" 
    echo -e "4) ${BOLD_WHITE}${ARROW_RIGHT} GitLab/Kanukyu ${BOOK_SYMBOL}" 
    echo -e "5) ${LIGHT_RED}${ARROW_RIGHT} Exit ${CROSS_MARK_EMOJI}"
    printf "${BOLD_YELLOW}${QUESTION_EMOJI} Enter your choice (1-5): ${RESET}"
    read -r choice
    case $choice in
        1) open_vm_tools ;;
        2) remove_vmware_tools ;;
        3) open_tiktok_kanukyu ;; 
        4) open_gitlab_kanukyu ;; 
        5) exit 0 ;;
        *) echo -e "${RED}${ERROR_EMOJI} Invalid choice. Please try again.${RESET}" && show_menu ;;
    esac
}

open_tiktok_kanukyu() {
    echo -e "${INFO_EMOJI} Opening TikTok page for Kanukyu..."
    xdg-open "https://www.tiktok.com/@kanukyu" 
    show_menu
}

open_gitlab_kanukyu() {
    echo -e "${INFO_EMOJI} Opening GitLab page for Kanukyu..."
    xdg-open "https://gitlab.com/kanukyu"  
    show_menu
}

open_vm_tools() {
    echo -e "${INFO_EMOJI}${BOLD_CYAN}Welcome to the VMware Tools Installation and Shared Folder Setup!${RESET}"
    sleep 2

    # Step 1: Install open-vm-tools and necessary packages
    echo -e "${INFO_EMOJI}${BOLD_YELLOW}Step 1:${RESET} Installing open-vm-tools and dependencies."
    sudo pacman -S --noconfirm open-vm-tools base-devel net-tools linux-headers devtools xf86-input-vmmouse xf86-video-vmware mesa
    echo -e "${SUCCESS_EMOJI}${LIGHT_GREEN}Installation completed successfully!${RESET}"
    sleep 2

    # Step 2: Start and enable the vmtoolsd and vmware-vmblock-fuse services
    echo -e "${INFO_EMOJI}${BOLD_YELLOW}Step 2:${RESET} Starting and enabling the vmtoolsd and vmware-vmblock-fuse services."
    sudo systemctl enable --now vmtoolsd.service vmware-vmblock-fuse.service
    echo -e "${SUCCESS_EMOJI}${LIGHT_GREEN}Services started and enabled successfully!${RESET}"
    sleep 2

    # Step 3: Configure Xorg for VMware
    echo -e "${INFO_EMOJI}${BOLD_YELLOW}Step 3:${RESET} Configuring Xorg settings to work with VMware."
    echo -e "${INFO_EMOJI}As a final step for Xorg, please go to VMware and select your virtual machine. In the settings, enable 3D graphics for your display, then restart the virtual machine. This will enhance graphics performance."
    sudo cp /etc/xdg/autostart/vmware-user.desktop /etc/xdg/autostart/
    sudo systemctl enable --now vmtoolsd.service
    echo -e "${SUCCESS_EMOJI}${LIGHT_GREEN}Xorg configuration completed!${RESET}"
    sleep 2

    # Step 4: Get shared folder name from the user
    echo -e "${INFO_EMOJI}${BOLD_YELLOW}Step 4:${RESET} Now, please provide the name of the shared folder you want to mount."
    printf "${QUESTION_EMOJI} Enter the Shared Folder Name: "
    read -r shared_folder
    # Validate input for shared folder name
    if [ -z "$shared_folder" ]; then
        echo -e "${ERROR_EMOJI}${RED}Error: No shared folder name provided. Please try again.${RESET}"
        open_vm_tools  # Restart the function to prompt the user again
        return
    fi

    # Step 5: Create directory for shared folder
    echo -e "${INFO_EMOJI}${BOLD_YELLOW}Step 5:${RESET} Creating the directory to mount the shared folder: $shared_folder."
    shared_folder_dir="/mnt/hgfs"
    sudo mkdir -p "$shared_folder_dir/$shared_folder"
    echo -e "${SUCCESS_EMOJI}${LIGHT_GREEN}Directory created successfully!${RESET}"
    sleep 2

    # Step 6: Mount the shared folder
    echo -e "${INFO_EMOJI}${BOLD_YELLOW}Step 6:${RESET} Attempting to mount the shared folder."
    if vmware-hgfsclient > /dev/null; then
        echo -e "${INFO_EMOJI}Shared folder '$shared_folder' detected. Proceeding to mount...${RESET}"
        sudo vmhgfs-fuse -o allow_other -o auto_unmount ".host:$shared_folder" "$shared_folder_dir/$shared_folder"
        echo -e "${SUCCESS_EMOJI}${LIGHT_GREEN}Shared folder '$shared_folder' mounted successfully!${RESET}"
    else
        echo -e "${WARNING_EMOJI}${YELLOW}No VMware shared folders found. Please ensure that shared folders are enabled in your VMware settings.${RESET}"
    fi
    sleep 2

    # Step 7: Add to /etc/fstab for automatic mounting
    echo -e "${INFO_EMOJI}${BOLD_YELLOW}Step 7:${RESET} Adding shared folder to /etc/fstab for automatic mounting at startup."
    echo ".host:/$shared_folder $shared_folder_dir/$shared_folder fuse.vmhgfs-fuse nofail,allow_other,x-systemd.automount 0 0" | sudo tee -a /etc/fstab
    echo -e "${SUCCESS_EMOJI}${LIGHT_GREEN}Fstab entry added successfully!${RESET}"
    sleep 2

    # Step 8: Create systemd service for the shared folder
    echo -e "${INFO_EMOJI}${BOLD_YELLOW}Step 8:${RESET} Creating a systemd service for the shared folder."
    shared_folder_service="/etc/systemd/system/mnt-hgfs-$shared_folder.service"
    sudo bash -c "cat > $shared_folder_service <<EOF
[Unit]
Description=Load VMware shared folder $shared_folder
Requires=vmware-vmblock-fuse.service
After=vmware-vmblock-fuse.service network-online.target
ConditionPathExists=.host:/$shared_folder
ConditionVirtualization=vmware
Wants=network-online.target

[Service]
Type=oneshot
RemainAfterExit=yes
ExecStart=/usr/bin/vmhgfs-fuse -o allow_other -o auto_unmount .host:/$shared_folder $shared_folder_dir/$shared_folder
ExecStop=/bin/umount $shared_folder_dir/$shared_folder

[Install]
WantedBy=multi-user.target
EOF"
    echo -e "${SUCCESS_EMOJI}${LIGHT_GREEN}Systemd service created successfully!${RESET}"
    sleep 2

    # Step 9: Enable systemd service
    sudo systemctl enable "$shared_folder_service"
    echo -e "${SUCCESS_EMOJI}${LIGHT_GREEN}Systemd service enabled successfully!${RESET}"
    sleep 2

    echo -e "${SUCCESS_EMOJI}${BOLD_CYAN}VMware Tools and shared folder setup is complete! Your shared folder '$shared_folder' is now ready for use.${RESET}"

    # Step 10: Verify utility status
    echo -e "${INFO_EMOJI}${BOLD_YELLOW}Step 10:${RESET} Verifying the status of VMware utilities to ensure proper functionality."
    utilities=("vmtoolsd" "vmware-checkvm" "vmware-toolbox-cmd" "vmware-user" "vmhgfs-fuse" "vmware-vmblock-fuse")

    for utility in "${utilities[@]}"; do
        if command -v "$utility" > /dev/null 2>&1; then
            echo -e "${SUCCESS_EMOJI}${LIGHT_GREEN}$utility is installed and available.${RESET}"
            # Additional runtime checks
            case $utility in
                "vmtoolsd")
                    systemctl is-active --quiet vmtoolsd.service && \
                    echo -e "${SUCCESS_EMOJI}${LIGHT_GREEN}vmtoolsd service is running.${RESET}" || \
                    echo -e "${ERROR_EMOJI}${RED}vmtoolsd service is not running. Check the service status.${RESET}"
                    ;;
                "vmware-user")
                    pgrep -x "vmware-user" > /dev/null && \
                    echo -e "${SUCCESS_EMOJI}${LIGHT_GREEN}vmware-user is running.${RESET}" || \
                    echo -e "${ERROR_EMOJI}${RED}vmware-user is not running.${RESET}"
                    ;;
                "vmhgfs-fuse")
                    mount | grep "vmhgfs" > /dev/null && \
                    echo -e "${SUCCESS_EMOJI}${LIGHT_GREEN}vmhgfs-fuse is mounted successfully.${RESET}" || \
                    echo -e "${WARNING_EMOJI}${YELLOW}vmhgfs-fuse is not mounted. Ensure shared folders are configured.${RESET}"
                    ;;
            esac
        else
            echo -e "${ERROR_EMOJI}${RED}$utility is not available. Please check your installation.${RESET}"
        fi
    done

    echo -e "${SUCCESS_EMOJI}${BOLD_CYAN}Utility status check completed!${RESET}"

    # Wait for user input before returning to menu
    echo -e "${INFO_EMOJI}${BOLD_YELLOW}Press any key to return to the menu...${RESET}"
    read -n 1 -s  # Waits for a single key press without displaying it

    show_menu
}

remove_vmware_tools() {
    echo -e "${INFO_EMOJI}${BOLD_CYAN}Welcome to the VMware Tools Uninstallation and Cleanup!${RESET}"
    sleep 2  # Wait for 2 seconds to let the user read

    # Stop and disable VMware-related services
    echo -e "${INFO_EMOJI}${BOLD_YELLOW}Step 1:${RESET} Stopping and disabling VMware-related services."
    sudo systemctl disable --now vmtoolsd.service vmware-vmblock-fuse.service
    echo -e "${SUCCESS_EMOJI}${LIGHT_GREEN}Services stopped and disabled successfully!${RESET}"
    sleep 2  # Wait for 2 seconds to let the user read

    # Get shared folder name from user
    echo -e "${INFO_EMOJI}${BOLD_YELLOW}Step 2:${RESET} Enter the shared folder name to clean up."
    printf "${QUESTION_EMOJI} Enter the Shared Folder Name: "
    read -r shared_folder
    
    # Validate input for shared folder name
    if [ -z "$shared_folder" ]; then
        echo -e "${ERROR_EMOJI}${RED}Error: No shared folder name provided. Exiting.${RESET}"
        return
    fi

    # Remove systemd service for the shared folder
    echo -e "${INFO_EMOJI}${BOLD_YELLOW}Step 3:${RESET} Removing the systemd service for the shared folder."
    shared_folder_service="/etc/systemd/system/mnt-hgfs-$shared_folder.service"
    if [ -f "$shared_folder_service" ]; then
        sudo systemctl disable "$shared_folder_service"
        sudo rm "$shared_folder_service"
        echo -e "${SUCCESS_EMOJI}${LIGHT_GREEN}Systemd service removed successfully!${RESET}"
    else
        echo -e "${WARNING_EMOJI}${YELLOW}No systemd service found for the shared folder. Skipping.${RESET}"
    fi
    sleep 2  # Wait for 2 seconds to let the user read

    # Remove shared folder directory
    echo -e "${INFO_EMOJI}${BOLD_YELLOW}Step 4:${RESET} Removing the shared folder directory."
    shared_folder_dir="/mnt/hgfs"
    if [ -d "$shared_folder_dir/$shared_folder" ]; then
        sudo rm -rf "$shared_folder_dir/$shared_folder"
        echo -e "${SUCCESS_EMOJI}${LIGHT_GREEN}Shared folder directory removed successfully!${RESET}"
    else
        echo -e "${WARNING_EMOJI}${YELLOW}Shared folder directory not found. Skipping.${RESET}"
    fi
    sleep 2  # Wait for 2 seconds to let the user read

    # Remove shared folder entry from /etc/fstab
    echo -e "${INFO_EMOJI}${BOLD_YELLOW}Step 5:${RESET} Removing shared folder entry from /etc/fstab."
    sudo sed -i "/.host:\/$shared_folder/d" /etc/fstab
    echo -e "${SUCCESS_EMOJI}${LIGHT_GREEN}Shared folder entry removed from /etc/fstab!${RESET}"
    sleep 2  # Wait for 2 seconds to let the user read

    # Uninstall open-vm-tools and related packages
    echo -e "${INFO_EMOJI}${BOLD_YELLOW}Step 6:${RESET} Uninstalling open-vm-tools and related packages."
    sudo pacman -Rns --noconfirm open-vm-tools xf86-input-vmmouse xf86-video-vmware mesa

    # Cleaning up residual files and directories
    echo -e "${INFO_EMOJI}${BOLD_YELLOW}Step 7:${RESET} Cleaning up residual files and directories."
    sudo rm -rf /etc/vmware-tools /usr/lib/systemd/system/vmtoolsd.service /usr/lib/systemd/system/vmware-vmblock-fuse.service /usr/lib/open-vm-tools
    echo -e "${SUCCESS_EMOJI}${LIGHT_GREEN}Residual files and directories cleaned up successfully!${RESET}"
    sleep 2  # Wait for 2 seconds to let the user read

    # Prompt user to reboot
    echo -e "${INFO_EMOJI}${BOLD_YELLOW}Step 8:${RESET} A reboot is recommended to complete the cleanup process."
    printf "${QUESTION_EMOJI} Would you like to reboot now? (yes/no): "
    read -r reboot_choice

    if [[ "$reboot_choice" =~ ^[Yy][Ee][Ss]|[Yy]$ ]]; then
        echo -e "${INFO_EMOJI}${BOLD_YELLOW}Rebooting the system now...${RESET}"
        sudo reboot
    else
        echo -e "${WARNING_EMOJI}${YELLOW}Reboot skipped. Please remember to reboot later to ensure all changes take effect.${RESET}"
    fi

    echo -e "${SUCCESS_EMOJI}${BOLD_CYAN}VMware Tools and shared folder cleanup is complete! Your system is now clean.${RESET}"
    show_menu
}


# Main script flow
main() {
    # Başlangıçta ana menüyü göster
    show_menu
}

# Ana akışın başlangıcında main fonksiyonunu çalıştır
main

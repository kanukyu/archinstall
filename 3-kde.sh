#!/bin/bash

# Color codes (Normal and Bold)
BLACK='\033[0;30m'        # Black
DARK_GRAY='\033[1;30m'    # Dark Gray
RED='\033[0;31m'          # Red
LIGHT_RED='\033[1;31m'    # Light Red
GREEN='\033[0;32m'        # Green
LIGHT_GREEN='\033[1;32m'  # Light Green
BROWN_ORANGE='\033[0;33m' # Brown/Orange
YELLOW='\033[1;33m'       # Yellow
BLUE='\033[0;34m'         # Blue
LIGHT_BLUE='\033[1;34m'   # Light Blue
PURPLE='\033[0;35m'       # Purple
LIGHT_PURPLE='\033[1;35m' # Light Purple
CYAN='\033[0;36m'         # Cyan
LIGHT_CYAN='\033[1;36m'   # Light Cyan
LIGHT_GRAY='\033[0;37m'   # Light Gray
WHITE='\033[1;37m'        # White
RESET='\033[0m'

# Secure the system by getting sudo permissions, updating the system, and welcoming the user to KDE, BTRFS, ZRAM, LightDM, and Neon Theme setup
function update_and_secure_system {
    echo -e "${WHITE}${BLUE}🎉 Welcome to the KDE, BTRFS, ZRAM, LightDM, and Neon Theme setup!${RESET}"
    echo -e "${CYAN}🔧 This script will guide you through setting up essential components for your system.${RESET}"
    
    echo -e "${YELLOW}🔐 Please enter your sudo password to update the system...${RESET}"
    sudo -v
    
    echo -e "${GREEN}⚙️ Updating system packages...${RESET}"
    sudo pacman -Syyu --noconfirm --needed 2>/dev/null
    echo -e "${LIGHT_GREEN}✅ System packages updated successfully!${RESET}"
    
    echo -e "${WHITE}${PURPLE}✨ Setup is complete! Your system is now secure and optimized.${RESET}"
    
    echo -e "${YELLOW}💡 Press [Enter] or [Space] to continue...${RESET}"
    read -n 1 -s
    echo
}

# Ask user for CPU type and install the appropriate microcode
function install_microcode {
    echo -e "${BOLD}${YELLOW}⚡️ Which CPU do you have? Choose one of the options:${RESET}"
    echo -e "${BLUE}1) Intel CPU${RESET}"
    echo -e "${GREEN}2) AMD CPU${RESET}"
    echo -e "${YELLOW}Please make your selection (1 or 2):${RESET}"
    read choice
    
    if [[ "$choice" == "1" ]]; then
        echo -e "${BOLD}${GREEN}✨ Intel CPU detected. Installing microcode...${RESET}"
        sudo pacman -S --noconfirm intel-ucode 2>/dev/null
        echo -e "${BOLD}${GREEN}✅ Intel microcode successfully installed!${RESET}"
    elif [[ "$choice" == "2" ]]; then
        echo -e "${BOLD}${BLUE}✨ AMD CPU detected. Installing microcode...${RESET}"
        sudo pacman -S --noconfirm amd-ucode 2>/dev/null
        echo -e "${BOLD}${BLUE}✅ AMD microcode successfully installed!${RESET}"
    else
        echo -e "${BOLD}${RED}❌ Invalid selection! Please respond with '1' or '2'.${RESET}"
    fi
}

# Install Plasma packages with progress bar
function install_plasma {
    echo -e "${BOLD}${YELLOW}⚙️ Installing Plasma environment and related packages...${RESET}"
    
    # Explanation of packages
    echo -e "${BOLD}${CYAN}📦 Before proceeding, here are the packages we'll install and why:${RESET}"
    
    echo -e "${WHITE}1) ${LIGHT_GREEN}plasma-desktop${RESET}: Core desktop environment for KDE Plasma. 🌟"
    echo -e "${WHITE}2) ${LIGHT_CYAN}xorg${RESET}: X.Org server and libraries for graphical environments. 🖥️"
    echo -e "${WHITE}3) ${BLUE}xorg-apps${RESET}: Essential utilities for X.Org. 🔧"
    echo -e "${WHITE}4) ${PURPLE}plasma-workspace${RESET}: Core workspace components for Plasma. 🖱️"
    echo -e "${WHITE}5) ${GREEN}plasma-workspace-wallpapers${RESET}: Plasma desktop wallpapers. 🖼️"
    echo -e "${WHITE}6) ${CYAN}dolphin${RESET}: KDE file manager. 📂"
    echo -e "${WHITE}7) ${YELLOW}konsole${RESET}: KDE terminal emulator. ⌨️"
    echo -e "${WHITE}8) ${RED}ark${RESET}: Archive manager for KDE. 📦"
    echo -e "${WHITE}9) ${LIGHT_BLUE}discover${RESET}: KDE software store for apps and updates. 🛒"
    echo -e "${WHITE}10) ${BROWN_ORANGE}spectacle${RESET}: Screenshot tool for KDE. 📸"
    echo -e "${WHITE}11) ${LIGHT_PURPLE}krdc${RESET}: Remote desktop client for KDE. 🌍"
    echo -e "${WHITE}12) ${LIGHT_GREEN}krfb${RESET}: KDE remote desktop server. 🔒"
    echo -e "${WHITE}13) ${LIGHT_CYAN}kgpg${RESET}: Encryption tool for KDE. 🔑"
    echo -e "${WHITE}14) ${LIGHT_BLUE}kfind${RESET}: Search tool for KDE. 🔍"
    echo -e "${WHITE}15) ${LIGHT_RED}parley${RESET}: Vocabulary trainer for KDE. 📚"
    echo -e "${WHITE}16) ${GREEN}ghostwriter${RESET}: Markdown editor for KDE. ✍️"
    echo -e "${WHITE}17) ${YELLOW}kget${RESET}: Download manager for KDE. ⬇️"
    echo -e "${WHITE}18) ${PURPLE}kalk${RESET}: KDE calculator. 🧮"
    echo -e "${WHITE}19) ${RED}gwenview${RESET}: Image viewer for KDE. 🖼️"
    echo -e "${WHITE}20) ${CYAN}okular${RESET}: Document viewer for KDE. 📑"

    echo -e "\n${LIGHT_BLUE}⚡ The installation will proceed now. It may take some time. Please wait...${RESET}"

    # Pause for user confirmation
    echo -e "${BOLD}${BLUE}Press ${WHITE}Enter${BLUE} or ${WHITE}Space${BLUE} to continue or ${RED}Ctrl+C${BLUE} to cancel.${RESET}"
    read -n 1 -s

    echo -e "${GREEN}✔️ Proceeding with the installation...${RESET}"

    # Install all packages in one go
    echo -e "${BOLD}${CYAN}✨ Installing packages...${RESET}"

    # Use pacman with --progress flag and silent output, show only progress
    sudo pacman -S --noconfirm --needed plasma-desktop xorg xorg-apps plasma-workspace plasma-workspace-wallpapers dolphin konsole ark discover spectacle krdc krfb kgpg kfind parley ghostwriter kget kalk gwenview okular >/dev/null 2>&1

    # Final confirmation
    echo -e "${BOLD}${GREEN}✅ All packages installed successfully!${RESET}"
}


# Function to configure X11 keymap with primary and secondary layouts, and system language
function configure_x11_keymap_and_language() {
    # Intro message
    echo -e "${BOLD}${YELLOW}🔧 Configure Your X11 Keyboard and Language Settings${RESET}"
    echo -e "${WHITE}------------------------------------------------------------${RESET}"
    echo -e "${CYAN}This tool allows you to set the keyboard layouts and system language for your X11 system. ${RESET}"
    echo -e "${CYAN}You can choose primary and secondary keyboard layouts and specify a language.${RESET}"
    echo -e "${WHITE}------------------------------------------------------------${RESET}"

    while true; do
        # Start with a header
        echo -e "${BOLD}${YELLOW}🎹 ${UNDERLINE}Available Countries and Keyboard Layouts:${RESET}"
        echo -e "${WHITE}------------------------------------------------------------${RESET}"

        # List the top 30 most popular keymaps with an option for custom input
        supported_keymaps=(
            "us" "uk" "tr" "de" "fr" "es" "it" "ru" "pl" "cz"
            "se" "no" "dk" "fi" "gr" "ro" "bg" "hr" "hu" "nl"
            "pt" "br" "ca" "sk" "si" "jp" "kr" "cn" "in" "lt"
        )

        # Display available layouts in a single line
        for i in "${!supported_keymaps[@]}"; do
            printf "${BLUE}%2d) ${WHITE}%s${RESET}  " "$((i + 1))" "${supported_keymaps[i]}"
        done
        echo -e "\n${BLUE}31)${RESET} ${WHITE}Custom layout${RESET}"
        echo -e "${WHITE}------------------------------------------------------------${RESET}"

        # Ask for primary layout
        echo -e "${YELLOW}💡 Please select your primary keyboard layout (1-31)${RESET}:"
        read -p "Primary Selection: " primary_selection

        if [[ "$primary_selection" =~ ^[1-9]$ ]] && ((primary_selection >= 1 && primary_selection <= 9)); then
            primary_keymap="${supported_keymaps[primary_selection-1]}"
            break
        elif [[ "$primary_selection" -ge 10 && "$primary_selection" -le 30 ]]; then
            primary_keymap="${supported_keymaps[primary_selection-1]}"
            break
        elif [ "$primary_selection" -eq 31 ]; then
            read -p "Enter your custom primary keyboard layout code: " primary_keymap
            break
        else
            echo -e "${RED}❌ Invalid selection for primary layout. Please try again.${RESET}"
        fi
    done

    while true; do
        # Ask for secondary layout
        echo -e "${YELLOW}💡 Please select your secondary keyboard layout (1-31)${RESET}:"
        read -p "Secondary Selection: " secondary_selection

        if [[ "$secondary_selection" =~ ^[1-9]$ ]] && ((secondary_selection >= 1 && secondary_selection <= 9)); then
            secondary_keymap="${supported_keymaps[secondary_selection-1]}"
            break
        elif [[ "$secondary_selection" -ge 10 && "$secondary_selection" -le 30 ]]; then
            secondary_keymap="${supported_keymaps[secondary_selection-1]}"
            break
        elif [ "$secondary_selection" -eq 31 ]; then
            read -p "Enter your custom secondary keyboard layout code: " secondary_keymap
            break
        else
            echo -e "${RED}❌ Invalid selection for secondary layout. Please try again.${RESET}"
        fi
    done

    # Set X11 keymap with primary and secondary layouts
    echo -e "${GREEN}✔️ Setting X11 keymap to '$primary_keymap' and '$secondary_keymap'...${RESET}"
    sudo localectl set-x11-keymap "$primary_keymap,$secondary_keymap" pc105 && \
        echo -e "${GREEN}✅ X11 keymap successfully set to '$primary_keymap' and '$secondary_keymap'.${RESET}"
        sleep 3

    while true; do
        # Language selection
        echo -e "${BOLD}${YELLOW}🌐 Available Languages:${RESET}"
        echo -e "${WHITE}------------------------------------------------------------${RESET}"
        supported_languages=("en_US.UTF-8" "tr_TR.UTF-8" "de_DE.UTF-8" "fr_FR.UTF-8" "es_ES.UTF-8" "it_IT.UTF-8" "ru_RU.UTF-8" "pl_PL.UTF-8" "zh_CN.UTF-8" "ja_JP.UTF-8")

        for i in "${!supported_languages[@]}"; do
            printf "${BLUE}%2d) ${WHITE}%s${RESET}  " "$((i + 1))" "${supported_languages[i]}"
        done
        echo -e "\n${BLUE}11)${RESET} ${WHITE}Custom language${RESET}"
        echo -e "${WHITE}------------------------------------------------------------${RESET}"

        echo -e "${YELLOW}💡 Please select your system language (1-11)${RESET}:"
        read -p "Language Selection: " language_selection

        if [[ "$language_selection" =~ ^[1-9]$ ]] && ((language_selection >= 1 && language_selection <= 10)); then
            language="${supported_languages[language_selection-1]}"
            break
        elif [ "$language_selection" -eq 11 ]; then
            read -p "Enter your custom language code (e.g., 'en_US.UTF-8'): " language
            break
        else
            echo -e "${RED}❌ Invalid language selection. Please try again.${RESET}"
        fi
    done

    # Apply language setting
    echo -e "${GREEN}✔️ Setting system language to '$language'...${RESET}"
    sudo localectl set-locale LANG="$language" && \
        echo -e "${GREEN}✅ System language successfully set to '$language'.${RESET}"
        sleep 3
}

function install_media() {
    # List of packages to install
    packages="networkmanager plasma-nm bluez bluedevil bluez-utils pipewire pipewire-pulse pipewire-audio pipewire-alsa wireplumber plasma-pa rtkit vlc"

    # Print welcoming message
    echo -e "${BOLD}${BLUE}✨ Welcome to the Media Installer! ✨${RESET}"
    echo -e "${WHITE}Installing essential media packages for your system...${RESET}"

    # Install packages
    echo -e "${BOLD}${YELLOW}🌐 Installing necessary packages...${RESET}"
    sudo pacman -S --noconfirm $packages >/dev/null 2>&1

    # Confirm installation of pipewire-jack
    yes | sudo pacman -S pipewire-jack >/dev/null 2>&1

    # Print success message
    echo -e "${GREEN}✔ Packages successfully installed!${RESET}"

    # Final message
    echo -e "${BOLD}${GREEN}🎉 All tasks completed! Your system is ready. Enjoy your KDE experience!${RESET}"
}

# Function to set up and enable ZRAM on Arch Linux
function setup_zram() {
    # Information about ZRAM
    echo -e "${CYAN}ZRAM is a technology that makes your system's RAM usage more efficient.${RESET}"
    echo -e "${CYAN}It creates a virtual swap space in RAM, providing much faster performance compared to disk-based swap.${RESET}"
    echo -e "${CYAN}By compressing data, it allows you to perform more tasks with less memory, and it is particularly useful on systems with limited RAM.${RESET}"
    echo -e "${CYAN}By setting up ZRAM, you can free up more space for tasks and achieve overall performance improvement.${RESET}"
    echo -e "${CYAN}Press ${WHITE}Enter${CYAN} or ${WHITE}Space${CYAN} to continue the installation or ${RED}Ctrl+C${CYAN} to cancel.${RESET}"

    # Wait for the user to press Enter or Space to continue
    read -n 1 -s

    echo -e "${GREEN}✔️ Proceeding with the installation...${RESET}"

    # Install zram-generator
    echo -e "${GREEN}✔️ Installing zram-generator...${RESET}"
    sudo pacman -S --noconfirm zram-generator 2>/dev/null
        echo -e "${GREEN}✅ zram-generator installed successfully.${RESET}"

    # Create the zram-generator configuration
    echo -e "${GREEN}✔️ Creating zram-generator configuration...${RESET}"
    sudo bash -c 'cat <<EOF > /etc/systemd/zram-generator.conf
[zram0]
zram-size = ram / 2
compression-algorithm = zstd
EOF' && \
        echo -e "${GREEN}✅ zram-generator configuration created successfully.${RESET}"

    # Reload systemd and enable the zram service
    echo -e "${GREEN}✔️ Reloading systemd and enabling zram service...${RESET}"
    sudo systemctl daemon-reload && \
        sudo systemctl enable --now systemd-zram-setup@zram0.service && \
        echo -e "${GREEN}✅ Zram service enabled and started.${RESET}"

    # Check the status of the zram service
    echo -e "${GREEN}✔️ Checking zram service status...${RESET}"
    sudo systemctl status systemd-zram-setup@zram0.service

    # Show the zram control information
    echo -e "${GREEN}✔️ Displaying zramctl output...${RESET}"
    zramctl

    # Display swap status
    echo -e "${GREEN}✔️ Displaying swap status...${RESET}"
    swapon --show

    # Final message
    echo -e "${CYAN}ZRAM has been successfully set up on your system! You can now enjoy improved performance.${RESET}"
    echo -e "${CYAN}Press ${WHITE}Enter${CYAN} to exit or ${RED}Ctrl+C${CYAN} to quit anytime during the process.${RESET}"
}

# Install necessary packages and configure grub-btrfs with Timeshift
function install_btrfs_tools {
    echo -e "${BOLD}${YELLOW}⚙️ Installing necessary packages for BTRFS and system management...${RESET}"
    
    # Explanation of packages
    echo -e "${BOLD}${CYAN}Before proceeding, here are the packages we'll install and why:${RESET}"
    
    echo -e "${WHITE}1) ${GREEN}btrfs-progs${RESET}: Tools for managing BTRFS filesystems."
    echo -e "${WHITE}2) ${GREEN}inotify-tools${RESET}: Provides tools to monitor file system events."
    echo -e "${WHITE}3) ${GREEN}grub-btrfs${RESET}: GRUB integration for BTRFS."
    echo -e "${WHITE}4) ${GREEN}timeshift${RESET}: System restore tool for Linux."

    echo -e "\n${YELLOW}The installation will proceed now. It may take some time.${RESET}"

    # Pause for user confirmation
    echo -e "${BOLD}${BLUE}Press ${WHITE}Enter${BLUE} or ${WHITE}Space${BLUE} to continue or ${RED}Ctrl+C${BLUE} to cancel.${RESET}"
    read -n 1 -s

    # Install all packages in one go
    echo -e "${BOLD}${CYAN}✨ Installing packages...${RESET}"

    # Use pacman to install necessary packages and show progress
    sudo pacman -S --noconfirm --needed btrfs-progs inotify-tools grub-btrfs timeshift 2>/dev/null

    echo -e "${BOLD}${CYAN}✨ Packages successfully installed!${RESET}"

    # Create the systemd service for grub-btrfsd
    echo -e "${BOLD}${CYAN}✨ Configuring grub-btrfsd service...${RESET}"
    sudo tee /etc/systemd/system/grub-btrfsd.service > /dev/null <<EOF
[Unit]
Description=Regenerate grub-btrfs.cfg

[Service]
Type=simple
LogLevelMax=notice
Environment="PATH=/sbin:/bin:/usr/sbin:/usr/bin"
ExecStart=/usr/bin/grub-btrfsd --syslog --timeshift-auto

[Install]
WantedBy=multi-user.target
EOF

    echo -e "${BOLD}${CYAN}✨ grub-btrfsd service configuration complete.${RESET}"

    # Reload systemd configuration and enable the service
    echo -e "${BOLD}${CYAN}✨ Reloading systemd daemon...${RESET}"
    sudo systemctl daemon-reload

    echo -e "${BOLD}${CYAN}✨ Enabling and starting grub-btrfsd service...${RESET}"
    sudo systemctl enable --now grub-btrfsd

    # Generate the GRUB config
    echo -e "${BOLD}${CYAN}✨ Generating GRUB config...${RESET}"
    sudo grub-mkconfig -o /boot/grub/grub.cfg

    # Check the status of the grub-btrfsd service
    echo -e "${BOLD}${CYAN}Checking the status of grub-btrfsd service...${RESET}"
    sudo systemctl status grub-btrfsd --no-pager

    # Check Timeshift status
    echo -e "${BOLD}${CYAN}Checking Timeshift status...${RESET}"
    sudo timeshift --check

    # Final confirmation
    echo -e "${BOLD}${GREEN}✅ All tools and services installed and configured successfully!${RESET}"
}

# Function to install git, clone yay from AUR, and build it with progress bar
function install_git_and_yay {
    # Installing git package first
    echo -e "${BOLD}${CYAN}⚙️ Installing Git...${RESET}"
    sudo pacman -S --noconfirm --needed git go 2>/dev/null

    echo -e "${BOLD}${GREEN}✅ Git installation completed!${RESET}"

    # Cloning yay from AUR and installing it
    echo -e "${BOLD}${CYAN}🔄 Cloning yay from AUR and building it...${RESET}"
    git clone https://aur.archlinux.org/yay.git | grep -E "Cloning into|Receiving objects"

    cd yay

    # Build and install yay with progress bar
    echo -e "${BOLD}${YELLOW}⚙️ Building and installing yay...${RESET}"
    yes | makepkg -si

    # Go back to previous directory
    cd

    echo -e "${BOLD}${GREEN}✅ Yay has been successfully installed!${RESET}"
}

# Function to install and configure LightDM, Node.js, npm, web-greeter, and lightdm-neon theme
function install_lightdm_and_neon {
    # Installing LightDM
    echo -e "${BOLD}${CYAN}⚙️ Installing LightDM...${RESET}"
    sudo pacman -S --needed --noconfirm lightdm 2>/dev/null

    # Enabling LightDM service
    echo -e "${BOLD}${CYAN}⚙️ Enabling LightDM service...${RESET}"
    sudo systemctl enable lightdm

    # Installing Node.js and npm
    echo -e "${BOLD}${CYAN}⚙️ Installing Node.js and npm from AUR...${RESET}"
    yay -Sy --needed --noconfirm nodejs npm
    echo -e "${BOLD}${GREEN}⚙️ Installed Node.js and npm from AUR...${RESET}"
    sleep 3
    # Installing web-greeter from AUR
    echo -e "${BOLD}${CYAN}⚙️ Installing web-greeter from AUR...${RESET}"
    yay -S --needed --noconfirm web-greeter
    echo -e "${BOLD}${GREEN}⚙️ Installed web-greeter from AUR...${RESET}"
    sleep 3
    # Cloning the LightDM Neon theme repository
    echo -e "${BOLD}${CYAN}🔄 Cloning LightDM Neon theme repository...${RESET}"
    git clone https://github.com/hertg/lightdm-neon.git && cd lightdm-neon

    # Building and installing LightDM Neon theme
    echo -e "${BOLD}${CYAN}⚙️ Building and installing LightDM Neon theme...${RESET}"
    make build | grep -E "building|installing"

    sudo make install
    echo -e "${BOLD}${GREEN}⚙️ Installed LightDM Neon theme...${RESET}"
    sleep 3
    # Going back to previous directory
    cd

    # Configuring LightDM greeter-session and theme
    echo -e "${BOLD}${CYAN}🔧 Configuring LightDM greeter-session and theme...${RESET}"
    sudo sed -i 's/#greeter-session=example-gtk-gnome/greeter-session=web-greeter/' /etc/lightdm/lightdm.conf
    sudo sed -i 's/theme: gruvbox/theme: neon/' /etc/lightdm/web-greeter.yml

    echo -e "${BOLD}${GREEN}✅ LightDM, Node.js, npm, web-greeter, and LightDM Neon theme have been successfully installed and configured!${RESET}"
}

# Function to ask user if they want to install GPU drivers
function ask_if_install_gpu_drivers() {
    echo -e "${BOLD}${YELLOW}Do you want to install GPU drivers? (y/n)${RESET}"
    read response

    if [[ "$response" == "y" || "$response" == "Y" ]]; then
        ask_and_install_gpu_drivers
    else
        echo -e "${GREEN}No GPU drivers will be installed.${RESET}"
    fi
}

# Function to determine GPU and install appropriate drivers
function ask_and_install_gpu_drivers() {
    echo -e "${BOLD}${BLUE}DETERMINING GPU TYPE...${RESET}"
    echo -e "${BOLD}${YELLOW}Available GPU types:${RESET}"
    
    # List available GPU types
    gpu_types=("NVIDIA" "AMD" "VMware" "VirtualBox")
    for i in "${!gpu_types[@]}"; do
        printf "%2d) %s\n" "$((i + 1))" "${gpu_types[i]}"
    done

    # Ask user for selection
    echo
    read -p "Select your GPU type (1-4): " selection

    # Validate and process selection
    if [[ "$selection" =~ ^[1-4]$ ]]; then
        gpu_type="${gpu_types[selection-1]}"
        case $gpu_type in
            "NVIDIA")
                install_nvidia_drivers
                ;;
            "AMD")
                install_amd_drivers
                ;;
            "VMware")
                install_vmware_drivers
                ;;
            "VirtualBox")
                install_virtualbox_drivers
                ;;
        esac
    else
        echo -e "${RED}Invalid selection. Please enter a number between 1 and 4.${RESET}"
    fi
}

# Function to install NVIDIA drivers
function install_nvidia_drivers() {
    local app_name="Nvidia Graphics"
    local nvidia_gpu="nvidia nvidia-settings lib32-nvidia-utils"
    
    echo -e "${BOLD}${BLUE}INSTALLING NVIDIA DRIVERS...${RESET}"

    # Loop through each package to check if it's installed
    for app in $nvidia_gpu; do
        if pacman -Q "$app" &>/dev/null; then
            echo -e "${GREEN}$app is already installed. Skipping.${RESET}"
        else
            echo -e "${YELLOW}$app is not installed. Installing...${RESET}"
        fi
    done

    # Install the missing NVIDIA drivers and related packages
    sudo pacman -Sy --needed --noconfirm $nvidia_gpu 2>/dev/null

    # Display installation success
    echo -e "${BOLD}${GREEN}$app_name drivers installed successfully!${RESET}"
}

# Function to install AMD drivers
function install_amd_drivers() {
    local app_name="AMD Graphics"
    local amd_gpu="xf86-video-amdgpu lib32-vulkan-radeon vulkan-radeon"
    
    echo -e "${BOLD}${BLUE}INSTALLING AMD DRIVERS...${RESET}"

    # Loop through each package to check if it's installed
    for app in $amd_gpu; do
        if pacman -Q "$app" &>/dev/null; then
            echo -e "${GREEN}$app is already installed. Skipping.${RESET}"
        else
            echo -e "${YELLOW}$app is not installed. Installing...${RESET}"
        fi
    done

    # Install the missing AMD drivers and related packages
    sudo pacman -Sy --needed --noconfirm $amd_gpu 2>/dev/null

    # Display installation success
    echo -e "${BOLD}${GREEN}$app_name drivers installed successfully!${RESET}"
}

# Function to install VMware drivers
function install_vmware_drivers() {
    local app_name="VMware GPU"
    local vmware_gpu="open-vm-tools xf86-video-vmware xf86-input-vmmouse mesa"
    
    echo -e "${BOLD}${BLUE}INSTALLING VMware GPU DRIVERS...${RESET}"

    # Loop through each package to check if it's installed
    for app in $vmware_gpu; do
        if pacman -Q "$app" &>/dev/null; then
            echo -e "${GREEN}$app is already installed. Skipping.${RESET}"
        else
            echo -e "${YELLOW}$app is not installed. Installing...${RESET}"
        fi
    done

    # Install the missing VMware drivers and related packages
    sudo pacman -Sy --needed --noconfirm $vmware_gpu 2>/dev/null

    # Enable VMware tools service
    sudo systemctl enable --now vmtoolsd.service

    # Display installation success
    echo -e "${BOLD}${GREEN}$app_name drivers installed successfully!${RESET}"
}

# Function to enable multilib repository and install Steam
function enable_multilib_and_install_steam() {
    # Ask user if they want to enable the multilib repository
    echo -e "${BOLD}${YELLOW}Do you want to enable the multilib repository? (y/n)${RESET}"
    read response

    if [[ "$response" == "y" || "$response" == "Y" ]]; then
        echo -e "${BOLD}${BLUE}ENABLING MULTILIB REPOSITORY...${RESET}"

        # Check if multilib is already enabled
        if grep -q "^\[multilib\]" /etc/pacman.conf; then
            echo -e "${GREEN}Multilib repository is already enabled.${RESET}"
        else
            sudo sed -i '/#\[multilib\]/,/#Include = \/etc\/pacman.d\/mirrorlist/ {
                s/#\[multilib\]/[multilib]/
                s/#Include = \/etc\/pacman.d\/mirrorlist/Include = \/etc\/pacman.d\/mirrorlist/
            }' /etc/pacman.conf
            sudo pacman -Syy 2>/dev/null
            echo -e "${BOLD}${GREEN}MULTILIB REPOSITORY ENABLED.${RESET}"

            # 5-second countdown before confirmation
            echo "Starting next operation in:"
            for i in {5..1}; do
                echo -e "${BOLD}${BLUE}$i...${RESET}"
                sleep 1
            done
        fi
    else
        echo -e "${GREEN}Multilib repository enabling skipped.${RESET}"
    fi

    # Ask user if they want to install Steam
    echo -e "${BOLD}${YELLOW}Do you want to install Steam? (y/n)${RESET}"
    read response

    if [[ "$response" == "y" || "$response" == "Y" ]]; then
        local app_name="Steam"
        local steam="steam"
        echo -e "${BOLD}${BLUE}INSTALLING STEAM...${RESET}"

        # Check if Steam is already installed
        if pacman -Q "$steam" &>/dev/null; then
            echo -e "${GREEN}Steam is already installed. Skipping installation.${RESET}"
        else
            sudo pacman -Sy --needed --noconfirm steam 
            echo -e "${BOLD}${GREEN}Steam installed successfully!${RESET}"
        fi
    else
        echo -e "${GREEN}Steam installation skipped.${RESET}"
    fi
}

# Function to install applications
function install_apps() {
    local apps=("fastfetch" "keepassxc" "discord" "firefox" "visual-studio-code-bin" "sublime-text")
    local app_names=("Fastfetch" "KeePassXC" "Discord" "Firefox" "Visual Studio Code" "Sublime Text")

    # Show available applications for installation
    echo -e "${BLUE}Which applications would you like to install? (Enter numbers separated by space)${RESET}"
    for i in "${!apps[@]}"; do
        echo -e "${YELLOW} $((i + 1))) ${app_names[i]}${RESET}"
    done

    # Read user input for app selection
    echo
    read -p "Enter the numbers corresponding to the applications you want to install: (e.g., 1 3 5 6)" selections
    selected_apps=()
    for num in $selections; do
        selected_apps+=("${apps[$((num - 1))]}")
    done

    # Show selected apps
    echo -e "${GREEN}You selected the following applications: ${selected_apps[@]}${RESET}"

    # Ask for confirmation
    read -p "Do you want to proceed with installation? (y/n): " confirmation
    if [[ "$confirmation" != "y" ]]; then
        echo -e "${RED}Installation canceled.${RESET}"
        return 0
    fi

    # Install selected applications
    for app in "${selected_apps[@]}"; do
        case $app in
            "fastfetch")
                echo -e "${BLUE}INSTALLING FASTFETCH...${RESET}"
                yay -Sy --needed --noconfirm fastfetch
                echo -e "${GREEN}Fastfetch installed successfully.${RESET}"
                ;;
            "keepassxc")
                echo -e "${BLUE}INSTALLING KEEPASSXC...${RESET}"
                yay -Sy --needed --noconfirm keepassxc
                echo -e "${GREEN}KeePassXC installed successfully.${RESET}"
                ;;
            "discord")
                echo -e "${BLUE}INSTALLING DISCORD...${RESET}"
                yay -Sy --needed --noconfirm discord
                echo -e "${GREEN}Discord installed successfully.${RESET}"
                ;;
            "firefox")
                echo -e "${BLUE}INSTALLING FIREFOX...${RESET}"
                yay -Sy --needed --noconfirm firefox
                echo -e "${GREEN}Firefox installed successfully.${RESET}"
                ;;
            "visual-studio-code-bin")
                echo -e "${BLUE}INSTALLING VISUAL STUDIO CODE...${RESET}"
                yay -Sy --needed --noconfirm visual-studio-code-bin
                echo -e "${GREEN}Visual Studio Code installed successfully.${RESET}"
                ;;
            "sublime-text")
                echo -e "${BLUE}INSTALLING SUBLIME TEXT...${RESET}"
                yay -Sy --needed --noconfirm sublime-text
                echo -e "${GREEN}Sublime Text installed successfully.${RESET}"
                ;;
        esac
    done
}

# Main script flow
function main() {
    # Welcome message
    echo -e "${BOLD}${CYAN}Welcome to the KDE, BTRFS, ZRAM, LightDM, and Neon Theme setup script!${RESET}"
    
    # Update and secure the system
    update_and_secure_system

    # Install microcode based on the user's CPU
    install_microcode

    # Install Plasma environment
    install_plasma

    # Configure X11 keymap and system language
    configure_x11_keymap_and_language

    # Install network, Bluetooth, audio/video, and VLC packages
    install_media

    # Set up and enable ZRAM
    setup_zram

    # Install and configure BTRFS tools and Timeshift
    install_btrfs_tools

    # Install Git, yay (AUR helper), and additional applications
    install_git_and_yay

    # Install and configure LightDM and Neon theme
    install_lightdm_and_neon

    # Ask the user if they want to install GPU drivers
    ask_if_install_gpu_drivers

    # Enable multilib repository and optionally install Steam
    enable_multilib_and_install_steam

    # Ask the user to select and install additional applications
    install_apps

    # Final message
    echo -e "${BOLD}${GREEN}🎉 All tasks completed! Your system is ready. Enjoy your KDE experience!${RESET}"
}

# Start the main script flow
main
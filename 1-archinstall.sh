#!/bin/bash

GREEN='\033[0;32m'
YELLOW='\033[1;33m'
BLUE='\033[0;34m'
RED='\033[0;31m'
CYAN='\033[1;36m'
RESET='\033[0m'

select_keyboard_layout() {
    # Giriş mesajı
    echo -e "\n${YELLOW}======================================================${RESET}"
    echo -e "${YELLOW}Welcome to the Arch Linux installation script made by kanukyu.${RESET}"
    echo -e "${YELLOW}This step allows you to set your keyboard layout.${RESET}"
    echo -e "${YELLOW}You can select a predefined layout or specify a custom one.${RESET}"
    echo -e "${YELLOW}The chosen layout will be active for this session.${RESET}"
    echo -e "${YELLOW}======================================================${RESET}\n"

    # Geçerli klavye düzenlerini dinamik olarak bulma
    local valid_layouts
    valid_layouts=$(find /usr/share/kbd/keymaps/ -type f -name "*.map.gz" | sed -E 's|.*/||;s|\.map\.gz||')

    while true; do
        # Mevcut klavye düzeni seçeneklerini görüntüleme
        echo -e "${YELLOW}Available predefined keyboard layouts:${RESET}"
        echo "1) United States (us)"
        echo "2) Turkey (trq)"
        echo "3) Germany (de)"
        echo "4) France (fr)"
        echo "5) Spain (es)"
        echo "6) United Kingdom (uk)"
        echo "7) Japan (jp)"
        echo "8) Russia (ru)"
        echo "9) Italy (it)"
        echo "10) Portugal (pt)"
        echo "11) Custom (Enter manually)"
        
        # Kullanıcıdan giriş alma (mesajdan sonra aynı satıra)
        echo -n -e "${YELLOW}Please select the keyboard layout (e.g., '1', '2'): ${RESET}"
        read -r layout_choice

        # Kullanıcı girdisini işleme ve düzeni ayarlama
        case $layout_choice in
            1) layout="us"; break ;;
            2) layout="trq"; break ;;
            3) layout="de"; break ;;
            4) layout="fr"; break ;;
            5) layout="es"; break ;;
            6) layout="uk"; break ;;
            7) layout="jp"; break ;;
            8) layout="ru"; break ;;
            9) layout="it"; break ;;
            10) layout="pt"; break ;;
            11)
                echo -e "${YELLOW}Please enter your custom keyboard layout code:${RESET}"
                read -r layout
                # Kullanıcı tarafından girilen özel düzenin geçerli olup olmadığını kontrol etme
                if echo "$valid_layouts" | grep -qw "$layout"; then
                    echo -e "${GREEN}Custom keyboard layout set to '$layout'.${RESET}"
                    break
                else
                    echo -e "${RED}Invalid keyboard layout code. Please try again.${RESET}"
                fi
                ;;
            *)
                echo -e "${RED}Invalid choice. Please try again.${RESET}" ;;
        esac
    done

    # Seçilen klavye düzenini yükleme
    loadkeys "$layout"
    echo -e "${GREEN}Keyboard layout set to '$layout' for this session.${RESET}"
    sleep 1
}

# Wi-Fi bağlantısını yapılandırma fonksiyonu
connect_wifi() {
    echo -e "${YELLOW}Attempting to connect to the Wi-Fi network...${RESET}"

    while true; do
        # Wi-Fi cihaz adı alınır (örneğin wlan0)
        device=$(iwctl station list | awk '{print $1}' | head -n 1)

        if [ -z "$device" ]; then
            echo -e "${RED}No Wi-Fi device found.${RESET}"
            echo -e "${YELLOW}Please ensure your Wi-Fi device is connected and try again.${RESET}"
            echo -e "${YELLOW}Returning to the main menu...${RESET}"
            return 1  # Ana menüye dön
        else
            break  # Cihaz bulunduysa döngüden çık
        fi
    done

    while true; do
        echo -e "${YELLOW}Available Wi-Fi networks:${RESET}"
        iwctl station "$device" scan  # Wi-Fi ağlarını tarar
        iwctl station "$device" get-networks | awk '/SSID/ { print NR-1, $2 }' | tail -n +2
        echo -e "${YELLOW}Please enter the Wi-Fi SSID or its number:${RESET}"
        read -r ssid_input

        if [[ "$ssid_input" =~ ^[0-9]+$ ]]; then
            ssid=$(iwctl station "$device" get-networks | awk "/SSID/ { print \$2 }" | sed -n "$((ssid_input+1))p")
        else
            ssid="$ssid_input"
        fi

        if [ -z "$ssid" ]; then
            echo -e "${RED}Invalid SSID selection. Please try again.${RESET}"
            continue
        fi

        echo -e "${GREEN}You selected SSID: $ssid${RESET}"
        echo -e "${YELLOW}1) Confirm and proceed${RESET}"
        echo -e "${YELLOW}2) Re-enter SSID${RESET}"
        echo -e "${YELLOW}3) Return to the main menu${RESET}"
        read -r ssid_choice
        case $ssid_choice in
            1) break ;;  # Girilen SSID ile devam et
            2) continue ;;  # SSID'yi yeniden gir
            3) return 1 ;;  # Ana menüye dön
            *) echo -e "${RED}Invalid selection. Please enter 1, 2, or 3.${RESET}" ;;
        esac
    done

    while true; do
        echo -e "${YELLOW}Please enter the Wi-Fi network password (leave blank for open network):${RESET}"
        read -r -s password  # Şifreyi sessizce oku

        # Wi-Fi ağına bağlanmayı dene
        iwctl --passphrase "$password" station "$device" connect "$ssid"

        # Bağlantının başarılı olup olmadığını kontrol et
        if iwctl station "$device" show | grep -q "Connected"; then
            echo -e "${GREEN}Successfully connected to the Wi-Fi network: $ssid.${RESET}"
            return 0
        else
            echo -e "${RED}Failed to connect to $ssid with the provided password.${RESET}"
            echo -e "${YELLOW}1) Try again${RESET}"
            echo -e "${YELLOW}2) Return to the main menu${RESET}"
            read -r choice
            case $choice in
                1) continue ;;  # Bağlantıyı tekrar dener
                2) return 1 ;;  # Ana menüye döner
                *) echo -e "${RED}Invalid selection. Please enter 1 or 2.${RESET}" ;;
            esac
        fi
    done
}

# Ethernet bağlantısını yapılandırma fonksiyonu
configure_ethernet() {
    while true; do
        echo -e "${YELLOW}Attempting to configure Ethernet connection...${RESET}"

        ip link show  # Mevcut ağ arabirimlerini göster

        echo -e "${YELLOW}Please enter your Ethernet interface name (e.g., 'eth0', 'ens33'): ${RESET}"
        read -r eth_interface  # Kullanıcı Ethernet arabirimi adını girer

        # Verilen arabirimin varlığını kontrol et
        if ! ip link show "$eth_interface" &> /dev/null; then
            echo -e "${RED}Error: Ethernet interface '$eth_interface' not found.${RESET}"
            echo -e "${YELLOW}1) Try again${RESET}"
            echo -e "${YELLOW}2) Return to the main menu${RESET}"
            read -r choice
            case $choice in
                1) continue ;;  # Ethernet yapılandırmasını yeniden dener
                2) return 1 ;;  # Ana menüye döner
                *) echo -e "${RED}Invalid selection. Please enter 1 or 2.${RESET}" ;;
            esac
        fi

        # Ethernet arabirimini etkinleştir
        ip link set "$eth_interface" up

        # DHCP ile IP adresi al
        dhcpcd "$eth_interface"

        # IP adresi alınıp alınmadığını kontrol et
        if ip addr show "$eth_interface" | grep -q "inet "; then
            echo -e "${GREEN}Ethernet connection successfully configured on $eth_interface.${RESET}"
            return 0
        else
            echo -e "${RED}Failed to configure Ethernet connection on $eth_interface.${RESET}"
            echo -e "${YELLOW}1) Try again${RESET}"
            echo -e "${YELLOW}2) Return to the main menu${RESET}"
            read -r choice
            case $choice in
                1) continue ;;  # Ethernet yapılandırmasını yeniden dener
                2) return 1 ;;  # Ana menüye döner
                *) echo -e "${RED}Invalid selection. Please enter 1 or 2.${RESET}" ;;
            esac
        fi
    done
}

# Ağ bağlantısını kontrol etme fonksiyonu
check_network_connection() {
    echo -e "${YELLOW}Checking network connection...${RESET}"

    # Ping ayarları: 2 deneme, her biri için 1 saniye bekleme süresi, toplamda 3 saniyelik zaman aşımı
    ping -c 2 -W 1 -w 3 archlinux.org > /dev/null 2>&1

    if [ $? -eq 0 ]; then
        echo -e "${GREEN}Network connection successful.${RESET}"
        return 0
    else
        echo -e "${RED}Network connection failed. Please check your connection settings.${RESET}"
        return 1
    fi
}

# Bağlantı seçimini yapma ve ağ yapılandırmasını başlatma fonksiyonu
handle_connection_choice() {
    echo -e "\n${YELLOW}======================================================${RESET}"
    echo -e "${YELLOW}Arch Linux Network Configuration${RESET}"
    echo -e "${YELLOW}This script will assist you in setting up a network connection.${RESET}"
    echo -e "${YELLOW}You can choose either Ethernet or Wi-Fi to establish a connection.${RESET}"
    echo -e "${YELLOW}Make sure your network device is properly connected before proceeding.${RESET}"
    echo -e "${YELLOW}======================================================${RESET}"
    echo ""
    while true; do
        echo -e "${YELLOW}1) Ethernet${RESET}"
        echo -e "${YELLOW}2) Wi-Fi${RESET}"
        echo -e "${YELLOW}Please select your connection type (e.g., '1', '2'): ${RESET}"
        read -r connection_choice  # Kullanıcı bağlantı türünü seçer

        case $connection_choice in
            1)
                configure_ethernet && check_network_connection && break
                ;;
            2)
                connect_wifi && check_network_connection && break
                ;;
            *)
                echo -e "${RED}Invalid selection. Please enter 1 or 2.${RESET}"
                ;;
        esac
    done

    echo -e "${GREEN}Connection successfully established!${RESET}"
}

update_ntp_settings() {
    declare -A ntp_servers=( 
        ["United States"]="us.pool.ntp.org 10000" ["Germany"]="de.pool.ntp.org 8000" ["China"]="cn.pool.ntp.org 7500"
        ["Brazil"]="br.pool.ntp.org 6500" ["India"]="in.pool.ntp.org 6000" ["Russia"]="ru.pool.ntp.org 5000"
        ["United Kingdom"]="uk.pool.ntp.org 4000" ["France"]="fr.pool.ntp.org 3500" ["Japan"]="jp.pool.ntp.org 3000"
        ["Australia"]="au.pool.ntp.org 2500" ["Canada"]="ca.pool.ntp.org 2400" ["Italy"]="it.pool.ntp.org 2300"
        ["Spain"]="es.pool.ntp.org 2200" ["Mexico"]="mx.pool.ntp.org 2100" ["Turkey"]="tr.pool.ntp.org 2000"
        ["South Korea"]="kr.pool.ntp.org 1900" ["Netherlands"]="nl.pool.ntp.org 1800" ["Poland"]="pl.pool.ntp.org 1700"
        ["Argentina"]="ar.pool.ntp.org 1600"
    )

    echo -e "${CYAN}Select your country to configure NTP settings:${RESET}"
    select country in "${!ntp_servers[@]}"; do
        if [[ -n "$country" ]]; then
            ntp_server="${ntp_servers[$country]}"
            server=$(echo $ntp_server | awk '{print $1}')
            users=$(echo $ntp_server | awk '{print $2}')
            echo -e "${GREEN}You selected: $country - NTP Server: $server with $users users${RESET}"
            break
        else
            echo -e "${RED}Invalid selection. Please choose a valid number (1-${#ntp_servers[@]}).${RESET}"
        fi
    done

    timesyncd_conf="/etc/systemd/timesyncd.conf"

    if [ -f "$timesyncd_conf" ]; then
        sudo sed -i "/^\[Time\]/,/^$/ {
            /^\(NTP\)=/d
            /^$/i\
NTP=$server
        }" "$timesyncd_conf" && echo -e "${GREEN}NTP settings updated.${RESET}" || echo -e "${RED}Error updating NTP settings.${RESET}"
    else
        echo -e "\n[Time]\nNTP=$server" | sudo tee "$timesyncd_conf" > /dev/null && echo -e "${GREEN}NTP settings updated.${RESET}" || echo -e "${RED}Error updating NTP settings.${RESET}"
    fi

    timedatectl set-ntp true && echo -e "${GREEN}NTP enabled.${RESET}" || echo -e "${RED}Error enabling NTP.${RESET}"
}

# Global Değişkenler
efi_partition=""
root_partition=""
disk_type=""

# Partitionları almak için kullanıcıdan bilgi iste
prompt_partitions() {
    printf "${YELLOW}----------------------------------${RESET}\n"
    printf "${GREEN}Device Information and Mount Status${RESET}\n"
    printf "${YELLOW}----------------------------------${RESET}\n"
    # Get the disk information
    lsblk -o NAME,SIZE,MODEL,MOUNTPOINT,FSTYPE,LABEL,TYPE
    # End of output
    # End of output
    printf "${YELLOW}----------------------------------${RESET}\n"
    printf "${RED}End of Disk Information${RESET}\n"
    printf "${YELLOW}----------------------------------${RESET}\n"

    # Kullanıcıdan EFI ve ROOT partitionlarını al
    echo -n -e "${GREEN}Please enter the EFI partition (e.g., 'sda1', 'nvme0n1p1'): ${RESET}"
    read -r efi_partition
    echo -e "${GREEN}EFI partition selected: $efi_partition${RESET}"
    
    echo -n -e "${GREEN}Please enter the ROOT partition (e.g., 'sda2', 'nvme0n1p2'): ${RESET}"
    read -r root_partition
    echo -e "${GREEN}ROOT partition selected: $root_partition${RESET}"
}

# Partitionları formatlamak
format_partitions() {
    echo -e "${YELLOW}Formatting EFI partition $efi_partition as FAT32...${RESET}"
    if mkfs.fat -F 32 /dev/$efi_partition; then
        echo -e "${GREEN}EFI partition formatted.${RESET}"
    else
        echo -e "${RED}Error formatting EFI partition.${RESET}"
        return 1
    fi

    echo -e "${YELLOW}Formatting ROOT partition $root_partition as Btrfs...${RESET}"
    if mkfs.btrfs -f /dev/$root_partition; then
        echo -e "${GREEN}ROOT partition formatted.${RESET}"
    else
        echo -e "${RED}Error formatting ROOT partition.${RESET}"
        return 1
    fi
}

# HDD veya SSD belirleme fonksiyonu
hdd_or_ssd() {
    lsblk -o NAME,SIZE,ROTA,MODEL | grep -v "loop" | while read -r line; do
        disk_name=$(echo $line | awk '{print $1}')
        disk_type=$(echo $line | awk '{print $3}')

        # Sadece disklerin root ve efi partitionlarını alıyoruz
        if [[ "$disk_name" =~ [0-9]+$ ]]; then  # sadece partition numarası olan diskleri alıyoruz
            # Disk türünü belirliyoruz
            if [[ "$disk_type" -eq 0 ]]; then
                disk_type="ssd"
            else
                disk_type="hdd"
            fi

            # Diskin ilk iki partitionunu (EFI ve ROOT) alıyoruz
            # Burada disk adının sonundaki sayılara göre partitionlar belirleniyor
            if [[ "$disk_name" =~ ^(nvme|sd)[a-z]+[0-9]+$ ]]; then
                root_partition="${disk_name}2"  # ROOT partition'ı genelde 2. sırada olur
                efi_partition="${disk_name}1"   # EFI partition'ı genelde 1. sırada olur

                # Disk türü ve partition bilgisi çıktısını yazdırıyoruz
                echo -e "${YELLOW}Disk: /dev/$disk_name${RESET}"
                echo -e "${YELLOW}Type: $disk_type${RESET}"
                mount_partitions "$efi_partition" "$root_partition" "$disk_type"
            fi
        fi
    done
}

# Partitionları mount etme
mount_partitions() {
    local efi_partition="$1"
    local root_partition="$2"
    local disk_type="$3"

    # SSD ve HDD için ayrı mount seçenekleri tanımla
    echo -e "${YELLOW}Preparing to mount the partitions...${RESET}"
    sleep 1
    
    if [ "$disk_type" == "ssd" ]; then
        # SSD için ROOT mount seçenekleri
        local root_mount_opts="rw,noatime,compress=zstd:1,ssd,space_cache=v2,discard,subvol=@"
        local home_mount_opts="rw,noatime,compress=zstd:1,ssd,space_cache=v2,discard,subvol=@home"
        # SSD için EFI mount seçenekleri
        local efi_mount_opts="noatime,ro,discard,nofail,flush,sync,utf8,uid=1000,gid=1000"
    else
        # HDD için ROOT mount seçenekleri
        local root_mount_opts="rw,noatime,compress=zstd:1,autodefrag,space_cache=v2,subvol=@"
        local home_mount_opts="rw,noatime,compress=zstd:1,autodefrag,space_cache=v2,subvol=@home"
        # HDD için EFI mount seçenekleri
        local efi_mount_opts="noatime,ro,sync,flush,utf8,uid=1000,gid=1000"
    fi

    # ROOT partition mount işlemi (ilk mount)
    echo -e "${YELLOW}Mounting ROOT partition on /mnt...${RESET}"
    mount /dev/$root_partition /mnt
    sleep 2

    # ROOT ve HOME subvolumes oluşturma
    echo -e "${YELLOW}Creating ROOT and HOME subvolumes...${RESET}"
    btrfs su cr /mnt/@ && btrfs su cr /mnt/@home
    umount /mnt
    sleep 3

    # ROOT partition mount işlemi (subvolumes ile)
    echo -e "${YELLOW}Mounting ROOT partition with selected options...${RESET}"
    mount -o $root_mount_opts /dev/$root_partition /mnt
    sleep 2

    # HOME subvolume mount işlemi
    echo -e "${YELLOW}Mounting HOME subvolume...${RESET}"
    mkdir -p /mnt/home
    mount -o $home_mount_opts /dev/$root_partition /mnt/home
    sleep 2

    # EFI partition mount işlemi
    echo -e "${YELLOW}Mounting EFI partition on /mnt/boot/efi...${RESET}"
    mkdir -p /mnt/boot/efi
    mount -o $efi_mount_opts /dev/$efi_partition /mnt/boot/efi
    sleep 2
}

install_base_system() {
    echo -e "\n${YELLOW}Updating package databases and installing base system...${RESET}\n"

    # Update archlinux-keyring and system before installing base packages
    echo -e "${YELLOW}Updating archlinux-keyring and system...${RESET}"
    pacman -Sy --noconfirm archlinux-keyring && pacman -Syu --noconfirm

    # Define the base system packages
    base_packages=(base base-devel linux linux-firmware util-linux iwd dhcpcd btrfs-progs dosfstools zstd micro nano grub efibootmgr efivar sudo networkmanager)

    # Update package databases
    pacman -Syy

    # Install the base system packages using pacstrap
    pacstrap /mnt "${base_packages[@]}"

    # Completion message
    echo -e "\n${GREEN}Base system installation completed.${RESET}\n"
    sleep 2
}

generate_fstab() {
    # Çerçeveli açıklama
    echo -e "\n${YELLOW}==============================================${RESET}"
    echo -e "${YELLOW}Generating fstab...${RESET}"
    echo -e "${YELLOW}==============================================${RESET}\n"

    # fstab dosyasının varlığını kontrol etme
    if [ ! -f /mnt/etc/fstab ]; then
        echo -e "${YELLOW}/mnt/etc/fstab dosyası bulunamadı. Yeni dosya oluşturuluyor...${RESET}"
        touch /mnt/etc/fstab
    fi

    # fstab dosyasındaki önceki düzenlemeleri kaldırma
    if grep -q 'subvol=@home' /mnt/etc/fstab; then
        echo -e "${YELLOW}Previous fstab adjustments found. Removing old entries...${RESET}"
        sed -i '/subvol=\/@home/d' /mnt/etc/fstab
        sleep 3
    fi

    # fstab dosyasını oluşturma
    genfstab -U /mnt | sed -E 's/,subvolid=[0-9]+(,| )/,subvol=\/@home\1/' >> /mnt/etc/fstab
    sleep 5

    # İşlem tamamlandığında kullanıcıyı bilgilendirme
    echo -e "\n${YELLOW}==============================================${RESET}"
    echo -e "${GREEN}fstab generated successfully.${RESET}"
    echo -e "${YELLOW}==============================================${RESET}\n"
    sleep 3
}

copy_scripts() {
    echo -e "\n${YELLOW}==============================================${RESET}"
    echo -e "${YELLOW}📂 Copying installation directory...${RESET}"
    echo -e "${YELLOW}==============================================${RESET}\n"

    local arch_install_dir="/mnt/archinstall"
    local source_dir="/root/archinstall"

    if ! touch /mnt/test_rw 2>/dev/null; then
        echo -e "${RED}❌ Error: /mnt is read-only! Remounting...${RESET}"
        mount -o remount,rw /mnt || { echo -e "${RED}⚠️ Failed to remount /mnt! Exiting...${RESET}"; exit 1; }
        echo -e "${GREEN}✅ Remounted /mnt as read-write.${RESET}"
    fi
    rm -f /mnt/test_rw

    if [ -d "$arch_install_dir" ]; then
        echo -e "${YELLOW}🗑️ Removing existing $arch_install_dir directory...${RESET}"
        rm -rf "$arch_install_dir"
        sleep 1
    fi

    echo -e "${YELLOW}📁 Creating $arch_install_dir directory...${RESET}"
    mkdir -p "$arch_install_dir" && echo -e "${GREEN}✅ Created $arch_install_dir directory.${RESET}" || {
        echo -e "${RED}❌ Error: Failed to create $arch_install_dir!${RESET}"
        exit 1
    }
    sleep 1

    if [ -d "$source_dir" ]; then
        cp -r "$source_dir"/* "$arch_install_dir/" && echo -e "${GREEN}✅ Successfully copied $source_dir to $arch_install_dir.${RESET}"
        
        chmod -R +x "$arch_install_dir"
        echo -e "${GREEN}✅ Set executable permissions for all scripts in $arch_install_dir.${RESET}"
    else
        echo -e "${YELLOW}⚠️ Warning: $source_dir does not exist, skipping...${RESET}"
    fi
    sleep 1

    echo -e "\n${YELLOW}==============================================${RESET}"
    echo -e "${GREEN}✅ Directory and scripts copied successfully.${RESET}"
    echo -e "${YELLOW}==============================================${RESET}\n"
    sleep 2
}

# Function to ask user if they want to enter the chroot environment with a countdown and stylish output
ask_enter_chroot() {
    # Stylish message before countdown - Pre Installation
    echo -e "\n${YELLOW}==============================================${RESET}"
    echo -e "${YELLOW}Pre Installation and The installation of Linux has completed.${RESET}"
    echo -e "${YELLOW}System configuration will proceed by entering the chroot environment.${RESET}"
    echo -e "${YELLOW}The 2nd script (2-chroot.sh) will be executed.${RESET}"
    echo -e "${YELLOW}==============================================${RESET}\n"

    # Countdown from 5 to 1 
    for i in {5..1}; do
        echo -e "${YELLOW}$i...${RESET}"  # Yellow countdown
        sleep 1
    done

    # Enter chroot and run the 2nd script
    echo -e "\n${YELLOW}==============================================${RESET}"
    echo -e "${GREEN}Entering the chroot environment and running the 2nd script...${RESET}"
    echo -e "${YELLOW}==============================================${RESET}\n"
    sleep 1

    # Doğru chroot komutunu kullan
    arch-chroot /mnt /bin/bash -c "/archinstall/2-chroot.sh"
}

# Main function to execute the script
main() {
    # 1. Select keyboard layout
    select_keyboard_layout

    # 2. Handle network connection choice (Wi-Fi or Ethernet)
    handle_connection_choice

    # 3. Update NTP settings (Network Time Protocol)
    update_ntp_settings
    
    # Adım 2: Kullanıcıdan partition adlarını al
    prompt_partitions

    # Adım 1: Disk türünü ve partitionları belirleme
    hdd_or_ssd

    # Adım 3: Partitionları formatla
    format_partitions
    
    # Adım 4: Partitionları mount et
    mount_partitions "$efi_partition" "$root_partition" "$disk_type"

    # Partitionları mount et
    # mount_partitions "$efi_partition" "$root_partition" "$disk_type"

    # 8. Install base system (Essential packages)
    install_base_system

    # 9. Generate fstab file (For mounting filesystems automatically at boot)
    generate_fstab

    # 10. Copy necessary scripts to the installation environment
    copy_scripts

    # 11. Enter chroot environment and run the second script
    ask_enter_chroot
}

# Execute the main function
main
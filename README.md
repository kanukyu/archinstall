# Arch Linux Installation Guide

Welcome to the ultimate Arch Linux installation guide. Follow these steps carefully to get Arch up and running on your system.

---

## **1. Set Keyboard Layout**
Replace `trq` with your preferred layout (e.g., `us`, `fr`, `es`):
```bash
loadkeys trq
```

---

## **2. Partitioning the Disks**
### Open the Partition Manager:
```bash
cfdisk
```
1. Select `gpt` as the partition table type.

### **Partition 1: EFI Filesystem**
1. Select `Free Space`.
2. Choose `New`, then type `5G`.
3. Go to `Type` (use the right arrow key) and select `EFI System`.

### **Partition 2: Linux Filesystem**
1. Select `Free Space` again.
2. Choose `New` and allocate the remaining space (press `Enter` to use all).

### **Write the Partitions**
1. Navigate to `Write` and press `Enter`.
2. Type `yes` to confirm.
3. Exit the partition manager.

### Verify Partitions:
```bash
lsblk
```

---

## **3. Sync Packages & Install Git**
```bash
pacman -Syy && pacman -S --noconfirm git
```

---

## **4. Clone Installation Scripts**
```bash
git clone https://gitlab.com/kanukyu/archinstall.git
cd archinstall
sudo chmod +x 1-install.sh 2-chroot.sh 3-after-arch-install.sh btrfs-snapshot.sh vmware_tools.sh pulseaudio_remover.sh pipewire_install.sh
```

---

## **5. Installation Process**
### **Run the Scripts**
Start by running the first script to initiate the installation. Once the second script (`2-chroot.sh`) finishes, you’ll be prompted to shut down your system and remove the Arch Linux installation media. For virtual machines, ensure you eject the ISO from the virtual CD-ROM.

```bash
./1-install.sh
```

### **Post-Installation Steps**
After completing `./1-install.sh` and `./2-chroot.sh`:
1. Shutdown your system:
    ```bash
    shutdown -h now
    ```
2. Remove the Arch Linux ISO or USB stick.  
   - If you’re using a virtual machine, detach the ISO from the virtual CD-ROM.
3. Reboot your system and run the third script to finalize the setup:
    ```bash
    cd archinstall
    ./3-after-installation.sh
    ```

---

## **6. Post-Installation Overview**
After completing the initial installation scripts, your system transforms into a fully-fledged Arch Linux setup with:

### **kde.sh Script: A Complete System Setup**
The script provides:
- Minimal **KDE Plasma** desktop environment installation.
- **Networking tools**: Ethernet, Wi-Fi, and Bluetooth support.
- **PipeWire** for superior audio performance.
- Essential productivity tools: LibreOffice, Firefox, Keepass, and Discord.
- **ZRAM swap configuration** for improved performance on low-memory systems.
- Developer tools: **Sublime Text** and **Visual Studio Code**.
- **GRUB and Btrfs integration**, including Timeshift support for snapshot management.
- **Yay package manager** for easy AUR package installation.
- X11/Wayland and **LightDM Web Greeter** with a Neon theme for modern aesthetics.
- Automatic configuration for keyboard layouts.
- Graphics driver support for NVIDIA, AMD, and virtual machines.
- Gaming-ready with **Steam installation**.


## **7. Customize LightDM Theme**
To install and customize the LightDM theme, check out:
[LightDM Neon on GitHub](https://github.com/hertg/lightdm-neon)

Screenshots:
![LightDM Screenshot 1](https://imgur.com/Nx3dToz.png)
![LightDM Screenshot 2](https://i.imgur.com/knZ4yT6.png)

---

## **8. Additional Notes**
- **Fastfetch Instead of Neofetch:** Use `fastfetch` for a faster and more stylish system info display.

## **Useful Links**
- [Btrfs Wiki](https://wiki.archlinux.org/title/Btrfs) - Official Arch wiki for setting up and using Btrfs.
- [Grub Btrfs](https://github.com/Antynea/grub-btrfs) - A useful tool for managing Btrfs with Grub.
- [Zram Wiki](https://wiki.archlinux.org/title/Zram) - Learn how to set up Zram for better system performance.
- [LightDM Neon Theme](https://github.com/hertg/lightdm-neon) - A fresh, modern LightDM theme to make your login screen look great.

## **9. TikTok Post**
Check out my TikTok post:
[Kanukyu's TikTok](https://www.tiktok.com/@kanukyu/video/7384701908390333702)

---

Enjoy your Arch Linux experience! **I use Arch, btw.** 😎


#!/bin/bash

# Kalın renkler için tput ile kodlar
BOLD_WHITE=$(tput bold; tput setaf 7)  # Beyaz
BOLD_BLUE=$(tput bold; tput setaf 4)   # Mavi
BOLD_ORANGE=$(tput bold; tput setaf 3) # Turuncu (ANSI'de doğrudan turuncu yok)
BOLD_MAGENTA=$(tput bold; tput setaf 5) # Mor
BOLD_YELLOW=$(tput bold; tput setaf 3)  # Sarı
BOLD_RED=$(tput bold; tput setaf 1)     # Kırmızı
WHITE=$(tput setaf 7)                   # Beyaz
NC=$(tput sgr0)  # Renk sıfırlama

KANUKYU="\
   ${BOLD_RED}__                  __             ${NC}
  ${BOLD_WHITE}/ /_____ ____  __ __/ /____ ____ __${NC}
 ${BOLD_YELLOW}/  '_/ _ \`/ _ \/ // /  '_/ // / // /${NC}
${BOLD_CYAN}/_/\_\\_,_/_//_/\_,_/_/\_\\_, /\_,_/ ${NC}
${BOLD_BLUE}                        /___/        ${NC}"

# Paketleri yükleme fonksiyonu
install_required_packages() {
    echo -e "${BOLD_CYAN}🔄 Installing required packages...${NC}"
    for package in "${REQUIRED_PACKAGES[@]}"; do
        if ! pacman -Qs "$package" > /dev/null; then
            echo -e "${BOLD_YELLOW}⚠️  Package $package is not installed. Installing...${NC}"
            sudo pacman -S --noconfirm "$package"
        else
            echo -e "${BOLD_WHITE}✅ Package $package is already installed.${NC}"
        fi
    done
}

# Menü fonksiyonu
menu() {
    echo -e "${BOLD_WHITE}$KANUKYU${NC}"
    echo -e "${BOLD_BLUE}=================================${NC}"
    echo -e "${BOLD_CYAN}   Disk Management Interface   ${NC}"
    echo -e "${BOLD_BLUE}=================================${NC}"
    echo -e "  ${BOLD_YELLOW}1.${NC} 🛠️  ${BOLD_YELLOW}Create partition from disk ${NC}  ${BOLD_YELLOW}▌▐█${NC}"
    echo -e "  ${BOLD_ORANGE}2.${NC} 🗑️  ${BOLD_ORANGE}Delete partition from disk ${NC}  ${BOLD_ORANGE}⛔☠️${NC}"
    echo -e "  ${BOLD_BLUE}3.${NC} ✏️  ${BOLD_BLUE}Change partition label ${NC}  ${BOLD_BLUE}📂➖${NC}"
    echo -e "  ${WHITE}4.${NC} 📂 ${BOLD_WHITE}List disks and partitions ${NC}  ${BOLD_WHITE}🔍🔑${NC}"
    echo -e "  ${BOLD_RED}5.${NC} ❌ ${BOLD_RED}Exit ${NC}  ${BOLD_RED}✂️⛔${NC}"
    echo -n -e "${BOLD_CYAN}Choose an option (1-5): ${NC}"
}

# Diskleri listeleme fonksiyonu
list_disks() {
    echo -e "${BOLD_YELLOW}=================================${NC}"
    echo -e "${BOLD_YELLOW}Disks and Partitions (Detailed):${NC}"
    echo -e "${BOLD_YELLOW}=================================${NC}"
    lsblk -o NAME,SIZE,TYPE,FSTYPE,UUID,LABEL,MOUNTPOINTS

    read -p "Do you want to continue? [Y]es or [N]o: " choice
    case "$choice" in
        [Yy]* ) echo -e "${BOLD_CYAN}Continuing...${NC}" ;;
        [Nn]* ) echo -e "${BOLD_RED}Exiting...${NC}"; exit 0 ;;
        * ) echo -e "${BOLD_YELLOW}⚠️  Invalid input. Please enter Y or N.${NC}"; list_disks ;;
    esac
}


# Partition creation function
create_partition() {
    echo -e "${GREEN}=================================${NC}"
    echo -e "${GREEN}Disk Management: Create New Partition${NC}"
    echo -e "${GREEN}=================================${NC}"
    
    # List current disks
    lsblk
    echo -e "${YELLOW}---------------------------------${NC}"
    
    # Get disk name from user
    read -p "Enter the disk to create partition (e.g., /dev/sda /dev/sdb /dev/nvme0n1): " disk
    if [ ! -b "$disk" ]; then
        echo -e "${RED}Error: The specified disk does not exist!${NC}"
        return 1
    fi

    # Check for mounted partitions
    mounted_partitions=$(lsblk -rno NAME,MOUNTPOINT | grep "^$(basename "$disk")" | awk '{if ($2 != "") print $1}')
    if [ -n "$mounted_partitions" ]; then
        echo -e "${RED}Error: The following partitions are mounted:${NC}"
        echo "$mounted_partitions"
        echo -e "${YELLOW}These partitions will be unmounted automatically.${NC}"
        for partition in $mounted_partitions; do
            sudo umount "/dev/$partition" || { echo -e "${RED}Error: Unable to unmount $partition.${NC}"; return 1; }
            echo -e "${CYAN}$partition has been unmounted.${NC}"
        done
    fi

    # Check for existing partitions
    existing_partitions=$(lsblk -rno NAME | grep "^$(basename "$disk")" | sed "s/^$(basename "$disk")//g" | grep -Eo '[0-9]+' | sort -n)
    if [ -z "$existing_partitions" ]; then
        echo -e "${GREEN}No existing partitions found on the disk.${NC}"
        echo -e "${GREEN}Partition table will be set to GPT.${NC}"
        initialize_gpt=true
        partition_number=1
    else
        echo -e "${CYAN}Existing partitions on the disk: $existing_partitions${NC}"
        last_partition=$(echo "$existing_partitions" | tail -n 1)
        partition_number=$((last_partition + 1))
        initialize_gpt=false
    fi
    echo -e "${YELLOW}Suggested partition number: $partition_number${NC}"

    # Get partition number from user
    read -p "Enter the partition number (default: $partition_number): " user_partition_number
    if [ -n "$user_partition_number" ]; then
        partition_number=$user_partition_number
    fi

    # Get partition size from user
    read -p "Enter the partition size (e.g., +10G or +9G): " size

    echo -e "${YELLOW}---------------------------------${NC}"
    echo -e "${CYAN}Processing disk: $disk${NC}"

    # Send commands to fdisk via a file
    {
        if $initialize_gpt; then
            echo g        # Create new GPT partition table
        fi
        echo n            # Create new partition
        echo "$partition_number"  # User-defined partition number
        echo              # Default first sector
        echo "$size"      # Create with specified size
        echo w            # Write changes
    } | sudo fdisk "$disk"
    
    echo -e "${YELLOW}---------------------------------${NC}"
    echo -e "${GREEN}Partition created, current state:${NC}"
    lsblk

    # Ask for format option
    echo -e "${CYAN}Choose file system to format the partition:${NC}"
    echo -e "${YELLOW}1.${NC} ext2"
    echo -e "${YELLOW}2.${NC} ext3"
    echo -e "${YELLOW}3.${NC} ext4"
    echo -e "${YELLOW}4.${NC} btrfs"
    echo -e "${YELLOW}5.${NC} exfat"
    echo -e "${YELLOW}6.${NC} fat"
    echo -e "${YELLOW}7.${NC} vfat"
    echo -e "${YELLOW}8.${NC} ntfs"
    echo -e "${YELLOW}9.${NC} swap"
    echo -e "${YELLOW}10.${NC} xfs"
    
    # Dual boot reminder for NTFS users
    echo -e "${RED}Warning:${NC} If you are using dual boot with Windows, please format the partition as NTFS."

    read -p "Enter your choice (1-10): " fs_choice

    # Determine partition and format
    partition="${disk}${partition_number}"

    case "$fs_choice" in
        1)
            echo -e "${CYAN}Formatting the partition as ext2: $partition${NC}"
            sudo mkfs.ext2 -F "$partition"
            echo -e "${GREEN}ext2 format completed!${NC}"
            ;;
        2)
            echo -e "${CYAN}Formatting the partition as ext3: $partition${NC}"
            sudo mkfs.ext3 -F "$partition"
            echo -e "${GREEN}ext3 format completed!${NC}"
            ;;
        3)
            echo -e "${CYAN}Formatting the partition as ext4: $partition${NC}"
            sudo mkfs.ext4 -F "$partition"
            echo -e "${GREEN}ext4 format completed!${NC}"
            ;;
        4)
            echo -e "${CYAN}Formatting the partition as btrfs: $partition${NC}"
            sudo mkfs.btrfs -f "$partition"
            echo -e "${GREEN}btrfs format completed!${NC}"
            ;;
        5)
            echo -e "${CYAN}Formatting the partition as exfat: $partition${NC}"
            sudo mkfs.exfat "$partition"
            echo -e "${GREEN}exfat format completed!${NC}"
            ;;
        6)
            echo -e "${CYAN}Formatting the partition as fat: $partition${NC}"
            sudo mkfs.fat -F 32 "$partition"
            echo -e "${GREEN}fat format completed!${NC}"
            ;;
        7)
            echo -e "${CYAN}Formatting the partition as vfat: $partition${NC}"
            sudo mkfs.vfat -F 32 "$partition"
            echo -e "${GREEN}vfat format completed!${NC}"
            ;;
        8)
            echo -e "${CYAN}Formatting the partition as ntfs: $partition${NC}"
            sudo mkfs.ntfs -f "$partition"
            echo -e "${GREEN}ntfs format completed!${NC}"
            ;;
        9)
            echo -e "${CYAN}Formatting the partition as swap: $partition${NC}"
            sudo mkswap "$partition"
            echo -e "${GREEN}swap format completed!${NC}"
            ;;
        10)
            echo -e "${CYAN}Formatting the partition as xfs: $partition${NC}"
            sudo mkfs.xfs -f "$partition"
            echo -e "${GREEN}xfs format completed!${NC}"
            ;;
        *)
            echo -e "${RED}Invalid choice! Format skipped.${NC}"
            ;;
    esac
}

change_label() {
    # Get block device from user
    echo -e "${GREEN}=================================${NC}"
    echo -e "${GREEN}Disk Management: Change label${NC}"
    echo -e "${GREEN}=================================${NC}"

    # List current partitions and labels using lsblk (only partitions)
    echo -e "${CYAN}Current partitions and labels:${NC}"
    lsblk -o NAME,LABEL,SIZE,TYPE -r | grep 'part'   # Hide non-partition items

    echo -n "Enter the block device (e.g., /dev/sdb): "
    read block_device

    # List partitions by name only
    partitions=$(lsblk -rno NAME,TYPE $block_device | awk '$2 == "part" {print $1}')   # Get only partition names

    if [ -z "$partitions" ]; then
        echo -e "${RED}No partitions found on $block_device.${NC}"
        return 1
    fi

    # Show partitions with numbers
    echo -e "${CYAN}Partitions found on $block_device:${NC}"
    counter=1
    partition_list=()
    for partition in $partitions; do
        partition_list+=($partition)
        echo "$counter) $partition"
        ((counter++))
    done

    # Ask user to select multiple partitions
    valid_input=false
    while [ "$valid_input" = false ]; do
        echo -n "Please select the partitions to change label (comma separated, e.g., 1, 2, 3): "
        read selections

        # Check the user's input selections
        selections=(${selections//,/ })  # Split by comma into an array
        valid_input=true
        selected_partitions=()

        # Validate each selection
        for selection in "${selections[@]}"; do
            if [[ "$selection" =~ ^[0-9]+$ ]] && [ "$selection" -ge 1 ] && [ "$selection" -le "${#partition_list[@]}" ]; then
                selected_partition=${partition_list[$((selection-1))]}
                selected_partitions+=($selected_partition)
            else
                echo -e "${RED}Invalid selection: $selection. Try again.${NC}"
                valid_input=false
                break
            fi
        done
    done

    # Change label for selected partitions
    for selected_partition in "${selected_partitions[@]}"; do
        echo -e "${GREEN}You selected: $selected_partition${NC}"

        # Ask user for the new label
        echo -n "Enter the new label for partition $selected_partition: "
        read new_label

        # Get file system type
        fs_type=$(lsblk -o FSTYPE /dev/$selected_partition | tail -n 1)

        # Check file system type and label
        case "$fs_type" in
            ext2|ext3|ext4)
                sudo e2label /dev/$selected_partition "$new_label"
                ;;
            btrfs)
                sudo btrfs filesystem label /dev/$selected_partition "$new_label"
                ;;
            exfat)
                sudo tune.exfat -L "$new_label" /dev/$selected_partition
                ;;
            fat|vfat)
                sudo fatlabel /dev/$selected_partition "$new_label"
                ;;
            ntfs)
                sudo ntfslabel /dev/$selected_partition "$new_label"
                ;;
            swap)
                sudo swaplabel -L "$new_label" /dev/$selected_partition
                ;;
            xfs)
                sudo xfs_admin -L "$new_label" /dev/$selected_partition
                ;;
            *)
                echo -e "${RED}Unsupported file system: $fs_type${NC}"
                continue
                ;;
        esac

        # Update disk structures
        sudo partprobe
        sudo udevadm trigger --subsystem-match=block

        # Check updated labels
        echo -e "${GREEN}Label successfully changed to: $new_label${NC}"
    done

    # Finally, list all partitions and labels
    echo -e "${CYAN}Updated partitions and labels:${NC}"
    lsblk -o NAME,LABEL
}


# Partition deletion function with wipefs (force) and fdisk fix
delete_partition() {
    echo -e "${GREEN}=================================${NC}"
    echo -e "${GREEN}Disk Management: Delete Partition${NC}"
    echo -e "${GREEN}=================================${NC}"
    
    # List current disks and partitions
    lsblk -o NAME,SIZE,TYPE,MOUNTPOINTS
    echo -e "${YELLOW}---------------------------------${NC}"

    # Select disk to delete partitions from
    read -p "Select a disk to delete partitions (e.g., /dev/sda /dev/sdb /dev/nvme0n1): " selected_disk
    if [[ ! -b $selected_disk ]]; then
        echo -e "${RED}Invalid disk name! Please select a correct disk.${NC}"
        return
    fi

    # List partitions on the selected disk
    echo -e "${CYAN}Existing partitions on the selected disk:${NC}"
    lsblk -o NAME,SIZE,TYPE,MOUNTPOINTS "$selected_disk" | grep "part"

    # Get partitions to delete from the user
    read -p "Enter the partitions to delete (e.g., 1 2 3): " partitions_to_delete

    # Delete selected partitions
    for part_num in $partitions_to_delete; do
        partition="${selected_disk}${part_num}"

        # Check if partition exists
        if [[ ! -b $partition ]]; then
            echo -e "${RED}Error: $partition does not exist.${NC}"
            continue
        fi

        # Unmount the partition if mounted
        echo -e "${CYAN}Unmounting $partition...${NC}"
        sudo umount "$partition" 2>/dev/null
        if [[ $? -eq 0 ]]; then
            echo -e "${CYAN}$partition successfully unmounted.${NC}"
        else
            echo -e "${RED}Error: $partition could not be unmounted or is already unmounted.${NC}"
        fi

        # Wipe filesystem signature from the partition using wipefs --force
        echo -e "${CYAN}Wiping filesystem signature from $partition...${NC}"
        sudo wipefs --all --force "$partition"
        if [[ $? -eq 0 ]]; then
            echo -e "${CYAN}Filesystem signature wiped from $partition.${NC}"
        else
            echo -e "${RED}Error: Failed to wipe signature from $partition.${NC}"
        fi

        # Delete partition using fdisk interactively
        echo -e "${YELLOW}---------------------------------${NC}"
        echo -e "${CYAN}$partition is being deleted...${NC}"
        sudo fdisk "$selected_disk" <<EOF
d
$part_num
w
EOF
    done

    # Show updated disk and partition list
    lsblk
}

# Main function to show the menu
main() {
    # Install required packages
    install_required_packages

    while true; do
        menu
        read -r choice

        case $choice in
            1)
                create_partition
                ;;
            2)
                delete_partition
                ;;
            3)
                change_label
                ;;
            4)
                list_disks
                ;;
            5)
                echo -e "${CYAN}Exiting...${NC}"
                exit 0
                ;;
            *)
                echo -e "${RED}Invalid choice! Please enter 1, 2, 3, 4, or 5.${NC}"
                ;;
        esac
    done
}

# Run the main function
main

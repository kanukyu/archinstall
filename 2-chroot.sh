#!/bin/bash

# Define global variables for EFI and root partitions
efi_partition=""
root_partition=""

# Color variables
GREEN='\033[0;32m'
YELLOW='\033[1;33m'
BLUE='\033[0;34m'
RED='\033[0;31m'
CYAN='\033[1;36m'
RESET='\e[0m'
BOLD_WHITE='\e[1;37m'
BOLD_LIGHT_BLUE='\e[1;94m'
BOLD_PURPLE='\e[1;35m'
BOLD_GREEN='\e[1;32m'
BOLD_BROWN='\e[0;33m'
BOLD_RED='\e[1;31m'
BOLD_YELLOW='\e[1;33m'

# select_timezone function
select_timezone() {
    echo -e "${CYAN}━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━${RESET}"
    echo -e "${GREEN}This function sets your system's timezone.${RESET}\n${YELLOW}1.${RESET} Select a timezone.\n${YELLOW}2.${RESET} Update the hardware clock.\n${YELLOW}3.${RESET} Apply changes to /etc/localtime.\n"
    echo -e "${CYAN}━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━${RESET}\n"
    
    while true; do
        echo -e "${YELLOW}1) Europe/Istanbul   2) America/New_York   3) Asia/Tokyo   4) Europe/London${RESET}"
        echo -e "${YELLOW}5) Australia/Sydney  6) Europe/Paris      7) Asia/Dubai   8) Africa/Cairo${RESET}"
        echo -e "${YELLOW}9) America/Los_Angeles  10) Custom Timezone  11) Exit${RESET}"

        # Kullanıcıdan seçim almak
        read -p "$(echo -e "${CYAN}Your choice: ${RESET}")" timezone_choice

        case $timezone_choice in
            1) zoneinfo="Europe/Istanbul" ;;
            2) zoneinfo="America/New_York" ;;
            3) zoneinfo="Asia/Tokyo" ;;
            4) zoneinfo="Europe/London" ;;
            5) zoneinfo="Australia/Sydney" ;;
            6) zoneinfo="Europe/Paris" ;;
            7) zoneinfo="Asia/Dubai" ;;
            8) zoneinfo="Africa/Cairo" ;;
            9) zoneinfo="America/Los_Angeles" ;;
            10)
                # Özel zaman dilimi girişi
                read -p "$(echo -e "${YELLOW}Enter a valid timezone (e.g., Continent/City): ${RESET}")" custom_timezone

                # Geçerliliği kontrol et
                if [[ -f "/usr/share/zoneinfo/$custom_timezone" ]]; then
                    zoneinfo="$custom_timezone"
                else
                    echo -e "${RED}Invalid timezone. Try again.${RESET}"
                    continue
                fi
                ;;
            11) 
                echo -e "${BLUE}Exiting timezone selection.${RESET}"
                return 
                ;;
            *) 
                echo -e "${RED}Invalid choice. Try again.${RESET}"
                continue 
                ;;
        esac

        # Zaman dilimi ayarlama ve donanım saati güncelleme
        ln -sf "/usr/share/zoneinfo/$zoneinfo" /etc/localtime && hwclock --systohc
        echo -e "${GREEN}✔ Timezone set to $zoneinfo and hardware clock updated.${RESET}"
        break
    done
}

select_language_region() {
    echo -e "${BLUE}━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━${RESET}\n${CYAN}This function will set your system's language and region.${RESET}\n${GREEN}Steps performed:${RESET}\n${YELLOW}1.${RESET} Update /etc/locale.gen for the selected locale.\n${YELLOW}2.${RESET} Generate the locale and set LANG environment variable.${RESET}\n${BLUE}━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━${RESET}\n"
    
    echo -e "${YELLOW}Select your language and region:${RESET}\n${CYAN}1) United States (English, US) - en_US.UTF-8\n2) Turkey (Turkish, TR) - tr_TR.UTF-8\n3) China (Chinese, CN) - zh_CN.UTF-8\n4) India (Hindi, IN) - hi_IN.UTF-8\n5) Germany (German, DE) - de_DE.UTF-8\n6) Brazil (Portuguese, BR) - pt_BR.UTF-8\n7) Russia (Russian, RU) - ru_RU.UTF-8\n8) Japan (Japanese, JP) - ja_JP.UTF-8\n9) France (French, FR) - fr_FR.UTF-8\n10) Spain (Spanish, ES) - es_ES.UTF-8\n11) Exit${RESET}\n"
    
    while true; do
        echo -n -e "${GREEN}Your choice: ${RESET}"; read -r locale_choice
        case $locale_choice in
            1) locale="en_US.UTF-8"; break ;;
            2) locale="tr_TR.UTF-8"; break ;;
            3) locale="zh_CN.UTF-8"; break ;;
            4) locale="hi_IN.UTF-8"; break ;;
            5) locale="de_DE.UTF-8"; break ;;
            6) locale="pt_BR.UTF-8"; break ;;
            7) locale="ru_RU.UTF-8"; break ;;
            8) locale="ja_JP.UTF-8"; break ;;
            9) locale="fr_FR.UTF-8"; break ;;
            10) locale="es_ES.UTF-8"; break ;;
            11) echo -e "${RED}Exiting language selection.${RESET}"; return ;;
            *) echo -e "${RED}Invalid choice. Please try again.${RESET}" ;;
        esac
    done

    echo -e "${YELLOW}Updating /etc/locale.gen and generating locale...${RESET}"
    sudo sed -i "s/^#${locale}/${locale}/" /etc/locale.gen
    locale-gen
    echo "LANG=$locale" > /etc/locale.conf
    echo -e "${GREEN}Localization updated. LANG set to '$locale'.${RESET}"
}

# set_keyboard_layout function
set_keyboard_layout() {
    echo -e "${CYAN}━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━${RESET}"
    echo -e "${GREEN}This function will set your system's keyboard layout.${RESET}"
    echo -e "${YELLOW}1.${RESET} The system keyboard layout will be set according to your choice."
    echo -e "${YELLOW}2.${RESET} The selected layout will be applied to the active console session in the chroot environment."
    echo -e "${YELLOW}3.${RESET} The layout will be saved persistently in ${BLUE}/etc/vconsole.conf${RESET} for future use."
    echo -e "${YELLOW}4.${RESET} If you choose 'Custom Keyboard Layout,' ensure you enter a valid layout."
    echo -e "${CYAN}━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━${RESET}\n"

    # Display current keyboard layout
    current_layout=$(localectl status | grep "VC Keymap" | awk '{print $3}')
    echo -e "${BLUE}Current keyboard layout:${RESET} ${current_layout}\n"

    # Keyboard layout selection loop
    while true; do
        echo -e "${CYAN}Please select the keyboard layout:${RESET}"
        echo -e "${BLUE}1)${RESET} United States (${YELLOW}us${RESET})"
        echo -e "${BLUE}2)${RESET} Turkey (${YELLOW}trq${RESET})"
        echo -e "${BLUE}3)${RESET} Germany (${YELLOW}de${RESET})"
        echo -e "${BLUE}4)${RESET} France (${YELLOW}fr${RESET})"
        echo -e "${BLUE}5)${RESET} Spain (${YELLOW}es${RESET})"
        echo -e "${BLUE}6)${RESET} United Kingdom (${YELLOW}gb${RESET})"
        echo -e "${BLUE}7)${RESET} Italy (${YELLOW}it${RESET})"
        echo -e "${BLUE}8)${RESET} Brazil (${YELLOW}br${RESET})"
        echo -e "${BLUE}9)${RESET} Enter a custom keyboard layout"
        echo -e "${BLUE}10)${RESET} Exit"

        # Fix: Properly quoting the read command
        read -p "$(echo -e "${CYAN}Enter your choice (1-10): ${RESET}")" layout_choice

        case $layout_choice in
            1) layout="us" ;;
            2) layout="trq" ;;
            3) layout="de" ;;
            4) layout="fr" ;;
            5) layout="es" ;;
            6) layout="gb" ;;
            7) layout="it" ;;
            8) layout="br" ;;
            9)
                read -p "$(echo -e "${YELLOW}Enter a valid keyboard layout (e.g., us, trq, de): ${RESET}")" custom_layout
                if loadkeys "$custom_layout" 2>/dev/null; then
                    layout="$custom_layout"
                else
                    echo -e "${RED}✘ Invalid.${RESET}"
                    continue
                fi
                ;;
            10)
                echo -e "${BLUE}Exiting keyboard layout selection.${RESET}"
                return
                ;;
            *)
                echo -e "${RED}✘ Invalid choice. Please try again.${RESET}"
                continue
                ;;
        esac

        echo -e "${GREEN}✔ $layout selected.${RESET}"
        loadkeys "$layout"
        echo "KEYMAP=$layout" > /etc/vconsole.conf
        echo -e "${GREEN}✔ Keyboard layout set to '${YELLOW}$layout${GREEN}' for the active console session in the chroot environment and saved persistently.${RESET}"
        break
    done
}

configure_hostname() {
    echo -e "${CYAN}━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━${RESET}"
    echo -e "${GREEN}This function will configure the system's hostname.${RESET}"
    echo -e "${YELLOW}Example:${RESET} 'mycomputer' → [user@mycomputer]$"
    echo -e "${CYAN}━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━${RESET}\n"

    while true; do
        # Prompt for hostname input
        read -p "$(echo -e "${CYAN}Enter hostname: ${RESET}")" hostname

        # Validate hostname
        if [[ "$hostname" =~ ^[a-zA-Z0-9_-]+$ ]]; then
            # Save the hostname and update /etc/hosts
            echo "$hostname" > /etc/hostname
            cat > /etc/hosts <<EOL
127.0.0.1 localhost
::1       localhost
127.0.1.1 $hostname.localdomain $hostname
EOL
            echo -e "${GREEN}✔ Hostname set to '$hostname'.${RESET}"

            # Ask if user wants to change hostname
            read -p "$(echo -e "${CYAN}Change hostname? (y/n): ${RESET}")" change_hostname
            if [[ "$change_hostname" =~ ^[Nn]$ ]]; then
                echo -e "${GREEN}Hostname configuration complete.${RESET}"
                break
            fi
        else
            echo -e "${RED}✘ Invalid hostname. Use only letters, numbers, '-' and '_'${RESET}"
        fi
    done
}

select_partition() {
    echo -e "${BLUE}━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━${RESET}"
    echo -e "${GREEN}EFI and Root Partition Selection${RESET}"
    echo -e "${BLUE}━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━${RESET}"
    
    # List EFI partitions (vfat and FAT32)
    echo -e "${YELLOW}EFI partition options (vfat and FAT32):${RESET}"
    efi_partitions=$(lsblk -o NAME,FSTYPE,FSVER,SIZE,LABEL -nr | awk '$2 == "vfat" && $3 == "FAT32" {print "/dev/" $1 " - " $4}')
    
    if [[ -n "$efi_partitions" ]]; then
        echo -e "${GREEN}$efi_partitions${RESET}"
        echo -e "${YELLOW}This partition is required for the GRUB bootloader to be installed in the EFI area.${RESET}"
        while true; do
            read -p "$(echo -e "${CYAN}Please select an EFI partition (e.g., sda1): ${RESET}")" efi_input
            efi_partition="/dev/$efi_input"
            if [[ -b "$efi_partition" ]]; then
                echo -e "${GREEN}Selected EFI partition: $efi_partition${RESET}"
                break
            else
                echo -e "${RED}✘ Invalid EFI partition. Please try again.${RESET}"
            fi
        done
    else
        echo -e "${RED}No valid EFI partitions found.${RESET}"
    fi

    # List root partitions (btrfs)
    echo -e "${YELLOW}Root partition options (btrfs):${RESET}"
    root_partitions=$(lsblk -o NAME,FSTYPE,FSVER,SIZE,LABEL -nr | awk '$2 == "btrfs" {print "/dev/" $1 " - " $4 " - " $3}')
    
    if [[ -n "$root_partitions" ]]; then
        echo -e "${GREEN}$root_partitions${RESET}"
        echo -e "${YELLOW}This partition will be usudo sed as the root filesystem and configured with mkinitcpio.${RESET}"
        while true; do
            read -p "$(echo -e "${CYAN}Please select a root partition (e.g., sda2): ${RESET}")" root_input
            root_partition="/dev/$root_input"
            if [[ -b "$root_partition" ]]; then
                echo -e "${GREEN}Selected root partition: $root_partition${RESET}"
                break
            else
                echo -e "${RED}✘ Invalid root partition. Please try again.${RESET}"
            fi
        done
    else
        echo -e "${RED}No valid root partitions found.${RESET}"
    fi
}

configure_mkinitcpio() {
    echo -e "${CYAN}━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━${RESET}"
    echo -e "${GREEN}Configuring mkinitcpio (initial RAM filesystem).${RESET}"
    echo -e "${YELLOW}Steps:${RESET} Detect root partition, verify filesystem type, update configuration, and rebuild initramfs."
    echo -e "${CYAN}━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━${RESET}\n"

    local mkinitcpio_conf="/etc/mkinitcpio.conf"
    local fs_type=$(findmnt -n -o FSTYPE "$root_partition")

    if [[ -z "$fs_type" ]]; then
        echo -e "${RED}✘ Filesystem type for ${YELLOW}$root_partition${RED} could not be detected.${RESET}"
        sleep 5; return 1
    fi

    echo -e "${GREEN}✔ Detected Root Partition:${RESET} $root_partition (${GREEN}Filesystem:${RESET} $fs_type)"
    
    if [[ "$fs_type" == "btrfs" ]]; then
        read -p "$(echo -e "${CYAN}Add 'btrfs' module to mkinitcpio? (y/n): ${RESET}")" add_btrfs
        [[ "$add_btrfs" =~ ^[Yy]$ ]] && sudo sed -i 's/^BINARIES=()/BINARIES=(btrfs)/' "$mkinitcpio_conf" \
            && echo -e "${GREEN}✔ 'btrfs' module added to mkinitcpio.${RESET}" || echo -e "${YELLOW}➤ Skipping 'btrfs' module addition.${RESET}"
    else
        echo -e "${YELLOW}➤ Filesystem is not btrfs. No modules will be added.${RESET}"
    fi

    echo -e "${BLUE}➤ Rebuilding initramfs...${RESET}"
    if mkinitcpio -P >/dev/null 2>&1; then
        echo -e "${GREEN}✔ mkinitcpio updated and images rebuilt successfully.${RESET}"
    else
        echo -e "${RED}✘ Failed to rebuild initramfs. Check the configuration.${RESET}"
    fi
    sleep 5
}

set_root_password() {
    echo -e "${CYAN}━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━${RESET}"
    echo -e "${YELLOW}➤ ${GREEN}Setting or changing the root password.${RESET}"
    echo -e "${YELLOW}➤ ${GREEN}You will be asked to enter and confirm the new password.${RESET}"
    echo -e "${CYAN}━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━${RESET}\n"

    while true; do
        # Kullanıcıdan şifre almak
        read -s -p "$(echo -e "${YELLOW}Enter new root password: ${RESET}")" root_password
        echo ""
        read -s -p "$(echo -e "${YELLOW}Confirm root password: ${RESET}")" root_password_confirm
        echo ""

        # Şifrelerin eşleşip eşleşmediğini kontrol et
        if [[ "$root_password" == "$root_password_confirm" ]]; then
            echo -e "${root_password}\n${root_password}" | passwd root > /dev/null 2>&1
            if [[ $? -eq 0 ]]; then
                echo -e "${GREEN}✔ Root password set successfully.${RESET}"
                break
            else
                echo -e "${RED}✘ Failed to set password. Please try again.${RESET}"
            fi
        else
            echo -e "${RED}✘ Passwords do not match. Please try again.${RESET}"
        fi
    done

    # Kullanıcıya şifreyi tekrar değiştirmek isteyip istemediğini sor
    while true; do
        read -p "$(echo -e "${YELLOW}Do you want to change the root password again? (y/n): ${RESET}")" change_root_password
        case "$change_root_password" in
            [Yy]*) 
                echo -e "${BLUE}➤ Restarting root password setup...${RESET}"
                set_root_password 
                break 
                ;;
            [Nn]*) 
                echo -e "${GREEN}✔ Root password setup complete.${RESET}"
                break 
                ;;
            *) 
                echo -e "${RED}✘ Invalid input. Please answer 'y' or 'n'.${RESET}" 
                ;;
        esac
    done
}

setup_networkmanager() {
    echo -e "${CYAN}━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━${RESET}\n${YELLOW}➤ ${GREEN}Setting up NetworkManager for network configuration.${RESET}\n${CYAN}━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━${RESET}\n"

    # Enable NetworkManager service to start at boot
    echo -e "${BLUE}➤ Enabling NetworkManager service...${RESET}"
    if systemctl enable NetworkManager &>/dev/null; then
        echo -e "${GREEN}✔ NetworkManager service enabled successfully.${RESET}"
    else
        echo -e "${RED}✘ Failed to enable NetworkManager service. Please check.${RESET}"
        return 1
    fi

    # Start NetworkManager service immediately
    echo -e "${BLUE}➤ Starting NetworkManager service...${RESET}"
    if systemctl start NetworkManager &>/dev/null; then
        echo -e "${GREEN}✔ NetworkManager service started successfully.${RESET}"
    else
        echo -e "${RED}✘ Failed to start NetworkManager service. Please check.${RESET}"
        return 1
    fi

    # Test network connectivity
    echo -e "${BLUE}➤ Testing network connectivity by pinging archlinux.org...${RESET}"
    if ping -c 1 archlinux.org &>/dev/null; then
        echo -e "${GREEN}✔ NetworkManager is active, and network is operational.${RESET}"
    else
        echo -e "${RED}✘ NetworkManager setup completed, but network connectivity test failed.${RESET}"
    fi

    echo -e "${CYAN}━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━${RESET}"
}

create_user() {
    echo -e "${CYAN}━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━${RESET}\n${YELLOW}➤ ${GREEN}Creating a new user and granting sudo permissions.${RESET}\n${CYAN}━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━${RESET}\n"

    echo -e "${BLUE}➤ Users with sudo permissions:${RESET}"
    grep -Po '^sudo.+:\K.*' /etc/group || echo -e "${RED}➤ No users with sudo permissions found.${RESET}\n"

    while true; do
        read -p "$(echo -e ${YELLOW}Enter username to create: ${RESET})" username
        if [[ "$username" =~ ^[a-zA-Z0-9_-]+$ ]]; then
            break
        else
            echo -e "${RED}➤ Invalid username. Use letters, numbers, '-' or '_'.${RESET}"
        fi
    done

    if id "$username" &>/dev/null; then
        echo -e "${RED}➤ User '$username' exists. Recreating.${RESET}"
        userdel -r "$username"
        echo -e "${GREEN}➤ User '$username' deleted.${RESET}"
    fi

    echo -e "${BLUE}➤ Creating user '${GREEN}$username${BLUE}'...${RESET}"
    useradd -m -G wheel "$username" && echo -e "${GREEN}✔ User '$username' created and added to 'wheel' group.${RESET}" || {
        echo -e "${RED}✘ Failed to create user '$username'. Exiting.${RESET}"; return;
    }

    while true; do
        read -sp "$(echo -e ${YELLOW}Enter password for $username: ${RESET})" user_password; echo
        read -sp "$(echo -e ${YELLOW}Confirm password for $username: ${RESET})" user_password_confirm; echo
        if [[ "$user_password" == "$user_password_confirm" ]]; then
            echo -e "$user_password\n$user_password" | passwd "$username" &>/dev/null && {
                echo -e "${GREEN}✔ Password set successfully for '$username'.${RESET}"; break;
            } || {
                echo -e "${RED}✘ Failed to set password. Try again.${RESET}";
            }
        else
            echo -e "${RED}✘ Passwords do not match. Try again.${RESET}"
        fi
    done

    echo -e "${BLUE}➤ Granting sudo permissions to ${GREEN}$username${BLUE}...${RESET}"
    echo "$username ALL=(ALL) ALL" | EDITOR='tee -a' visudo > /dev/null && echo -e "${GREEN}✔ Sudo permissions granted to '$username'.${RESET}" || echo -e "${RED}✘ Failed to grant sudo permissions.${RESET}"

    while true; do
        read -p "$(echo -e "${YELLOW}New user is '$username'. Change it? (y/n): ${RESET}")" change_user
        case "$change_user" in
            [Yy]*) create_user; break;;
            [Nn]*) echo -e "${GREEN}✔ User creation finalized.${RESET}"; break;;
            *) echo -e "${RED}✘ Invalid input. Use 'y' or 'n'.${RESET}";;
        esac
    done
}

install_grub() {
    echo -e "${BLUE}━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━${RESET}"
    echo -e "${YELLOW}➤ ${GREEN}This function will install and configure the GRUB bootloader on your system.${RESET}"
    echo -e "${YELLOW}➤ ${GREEN}It will automatically detect your EFI and root partitions, and set up the bootloader accordingly.${RESET}"
    echo -e "${BLUE}━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━${RESET}"
    echo ""

    # Select system type (EFI or Legacy BIOS)
    echo -e "${YELLOW}➤ Please choose your system type:${RESET}"

    # Instead of using PS3 with color codes, display it manually
    echo -e "${GREEN}1) EFI system${RESET}"
    echo -e "${GREEN}2) Legacy BIOS system${RESET}"
    
    # Manually prompt for user input with colored prompt
    printf "%b" "${BOLD_GREEN}Select an option: ${RESET}"
    read -r selection

    case $selection in
        1)
            echo -e "${YELLOW}➤ Detected EFI partition: ${GREEN}$efi_partition${RESET}"

            # Ensure /boot/efi exists
            if [ ! -d "/boot/efi" ]; then
                echo -e "${YELLOW}➤ Creating mount point /boot/efi${RESET}"
                mkdir -p /boot/efi
                echo -e "${GREEN}➤ Successfully created /boot/efi.${RESET}"
            fi

            # Mount the EFI partition
            echo -e "${YELLOW}➤ Mounting EFI partition ${GREEN}$efi_partition ${YELLOW}to /boot/efi...${RESET}"
            mount -o rw,remount "$efi_partition" /boot/efi
            echo -e "${GREEN}➤ Mounted $efi_partition to /boot/efi successfully.${RESET}"

            # Automatically add Btrfs support to GRUB
            echo -e "${YELLOW}➤ Adding Btrfs support to GRUB...${RESET}"
            sudo sed -i '/^GRUB_PRELOAD_MODULES=/c\GRUB_PRELOAD_MODULES="btrfs"' /etc/default/grub
            echo -e "${GREEN}➤ Btrfs support added to GRUB configuration.${RESET}"

            # Install GRUB for EFI system
            echo -e "${BLUE}➤ Installing GRUB for EFI system...${RESET}"
            grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id="ARCH_EFI_GRUB" --removable --recheck
            echo -e "${GREEN}➤ GRUB installation for EFI system completed successfully!${RESET}"
            ;;

        2)
            # For Legacy BIOS, install GRUB on the entire disk (not just the partition)
            disk="${root_partition%%[0-9]*}"  # Remove partition number to get the disk name
            echo -e "${GREEN}➤ Detected root partition: ${YELLOW}$root_partition${RESET}"

            # Automatically add Btrfs support to GRUB
            echo -e "${YELLOW}➤ Adding Btrfs support to GRUB...${RESET}"
            sudo sed -i '/^GRUB_PRELOAD_MODULES=/c\GRUB_PRELOAD_MODULES="btrfs"' /etc/default/grub
            echo -e "${GREEN}➤ Btrfs support added to GRUB configuration.${RESET}"
            # Install GRUB for Legacy BIOS
            echo -e "${BLUE}➤ Installing GRUB for Legacy BIOS system (to disk ${YELLOW}$disk${RESET})...${RESET}"
            grub-install --target=i386-pc "$disk"
            echo -e "${GREEN}➤ GRUB installation for Legacy BIOS system completed successfully!${RESET}"
            ;;

        *)
            echo -e "${RED}➤ Invalid selection. Please choose either 1 or 2.${RESET}"
            ;;
    esac

    # Generate GRUB configuration
    echo -e "${YELLOW}➤ Generating GRUB configuration file...${RESET}"
    grub-mkconfig -o /boot/grub/grub.cfg
    echo -e "${GREEN}➤ GRUB configuration file generated successfully!${RESET}"
}

# Function to move files from /archinstall to /home/$user/archinstall with permissions set
move_archinstall() {
    local user
    echo -e "${CYAN}━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━${RESET}"
    echo -e "${GREEN}This function moves files from /archinstall to /home/\$user/archinstall.${RESET}"
    echo -e "${GREEN}It will create the necessary directory, set permissions, and ensure scripts can be executed.${RESET}"
    echo -e "${CYAN}━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━${RESET}\n"

    # Prompt for username
    printf "%b" "${BOLD_YELLOW}Please enter the username: ${RESET}"
    read -r user
    if [ -z "$user" ]; then
        echo -e "${RED}Username cannot be empty. Exiting...${RESET}"
        return 1
    fi

    # Check if user exists
    if ! id "$user" &>/dev/null; then
        echo -e "${RED}User '$user' does not exist. Exiting...${RESET}"
        return 1
    fi

    # Check if /archinstall exists
    if [ ! -d "/archinstall" ]; then
        echo -e "${RED}/archinstall directory does not exist. Please ensure the directory is available.${RESET}"
        return 1
    fi

    echo -e "${YELLOW}Moving files from /archinstall to /home/$user/archinstall...${RESET}"

    # Ensure target directory is clean and create new directory
    rm -rf "/home/$user/archinstall" && echo -e "${YELLOW}/home/$user/archinstall removed if existed.${RESET}"
    mkdir -p "/home/$user/archinstall" && echo -e "${YELLOW}Created /home/$user/archinstall.${RESET}"

    # Set ownership and permissions for directory and its content
    chown -R "$user:$user" "/home/$user/archinstall" && chmod -R 755 "/home/$user/archinstall" && echo -e "${GREEN}Permissions set for /home/$user/archinstall.${RESET}"

    # Move files with error handling
    if mv /archinstall/* "/home/$user/archinstall/"; then
        echo -e "${GREEN}Files moved successfully.${RESET}"
    else
        echo -e "${RED}Error: Failed to move files from /archinstall.${RESET}"
        return 1
    fi

    # Clean up original directory
    rm -rf /archinstall && echo -e "${GREEN}Original /archinstall directory removed.${RESET}"

    # Ensure script execution permissions
    chmod -R +x "/home/$user/archinstall" && echo -e "${GREEN}Execute permissions applied to all files.${RESET}"

    # Final confirmation
    echo -e "${GREEN}You can now run scripts from /home/$user/archinstall.${RESET}"
    sleep 5
}

# Finalize the installation and provide shutdown instructions
finalize_installation() {
    # Completion message and installation finalization notification
    echo -e "\e[32mArch Linux installation is complete.\e[0m"  # Green success message
    echo -e "\e[33mPlease remove the installation media.\e[0m"  # Yellow reminder to remove media
    echo -e "\e[31mNow, to shut down the system, please follow these steps:\e[0m"  # Red shutdown instructions
    
    # Run the umount command
    echo -e "\e[33mUnmounting all partitions...\e[0m"  # Yellow status message for unmounting
    umount -R /mnt  # Unmount all partitions
    echo -e "\e[32mAll partitions unmounted.\e[0m"  # Green success message after unmounting
    
    # Shutdown instructions
    echo -e "\e[31mFirst, exit the chroot environment by typing:\e[0m"  # Red exit chroot message
    echo -e "\e[32mexit\e[0m"  # Green 'exit' command
    echo -e "\e[31mThen, shut down the system with:\e[0m"  # Red shutdown message
    echo -e "\e[32mshutdown -h now\e[0m"  # Green shutdown command
}

# Main function to execute the script
main() {
    set_keyboard_layout
    select_language_region
    select_timezone
    configure_hostname
    select_partition
    configure_mkinitcpio
    set_root_password
    setup_networkmanager
    create_user
    install_grub
    move_archinstall
    finalize_installation
}

# Execute the main function
main